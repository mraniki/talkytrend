# CHANGELOG



## v1.14.14 (2023-09-09)

### Other

* Merge pull request #141 from mraniki/dev

👷 CI Change ([`de1dc72`](https://github.com/mraniki/talkytrend/commit/de1dc72cd783db49d6b574daa9d2c401862e6950))

### 👷

* 👷 CI Change ([`2f37660`](https://github.com/mraniki/talkytrend/commit/2f37660fb34e6d8e680355c4a07fc8d436795e5d))


## v1.14.13 (2023-09-07)

### Other

* Merge pull request #140 from mraniki/dev

Merge pull request #139 from mraniki/main ([`25d168a`](https://github.com/mraniki/talkytrend/commit/25d168a1b5da8b73829b835e2dabd208f2268606))

* Merge branch &#39;main&#39; into dev ([`2cb2058`](https://github.com/mraniki/talkytrend/commit/2cb2058a0da1cc1e13005dc7ca2c6930295eb243))

### Update

* Update README.md ([`b66ff71`](https://github.com/mraniki/talkytrend/commit/b66ff7137e02862e672587e31b150afac5834f4f))


## v1.14.12 (2023-09-07)

### Other

* Merge pull request #139 from mraniki/main

Merge pull request #138 from mraniki/dev ([`f611759`](https://github.com/mraniki/talkytrend/commit/f6117597ab61306d25d5bb911985e04a0b2938ca))

* Merge pull request #138 from mraniki/dev

🔥 cleanup import ([`ce25a68`](https://github.com/mraniki/talkytrend/commit/ce25a6807685da8ba10bc6402b66bb07815d9c9f))

* Merge branch &#39;main&#39; into dev ([`b72fad2`](https://github.com/mraniki/talkytrend/commit/b72fad2b6f338bf93508ffff4cdaf80f2f394538))

### Update

* Update README.md ([`afc1603`](https://github.com/mraniki/talkytrend/commit/afc16033b7c000bd9d58fe8664ca6fb4fa24cb5f))

* Update Requirements ([`844ef6c`](https://github.com/mraniki/talkytrend/commit/844ef6c430adcce3e0c254ae629c9a74f7eb44fa))

### ⬆️

* ⬆️ ([`6b17bc7`](https://github.com/mraniki/talkytrend/commit/6b17bc73269a8b2d52ad37f04c6e6fe60ba8df8b))

### 🔥

* 🔥 cleanup import ([`353f90c`](https://github.com/mraniki/talkytrend/commit/353f90c54b990cfec41ff816e91f91c63ffb04ff))

* 🔧 🔥 ([`2fb2821`](https://github.com/mraniki/talkytrend/commit/2fb2821bee65fad2c5663842e91a9ed6ca3fb51f))


## v1.14.11 (2023-09-03)

### Other

* Merge pull request #137 from mraniki/renovate/ruff-0.x

⬆️ 🛠️(deps): update dependency ruff to &gt;=0.0.287,&lt;0.0.288 ([`8aeeaea`](https://github.com/mraniki/talkytrend/commit/8aeeaea93d5f3901874435945acf815f5cc2aa24))

* Merge pull request #136 from mraniki/dependabot/pip/dev/sphinx-7.2.5

⬆️ 🤖 Dependencies: Update sphinx requirement from 7.2.4 to 7.2.5 ([`b6fb361`](https://github.com/mraniki/talkytrend/commit/b6fb3612c3305601d8535bb577088f64db02e0c7))

### ⬆️

* ⬆️ 🛠️(deps): update dependency ruff to &gt;=0.0.287,&lt;0.0.288 ([`bdc272b`](https://github.com/mraniki/talkytrend/commit/bdc272b42d373557db7e6c5618a2eb2262f37ca2))


## v1.14.10 (2023-08-31)

### Other

* Merge pull request #135 from mraniki/renovate/sphinx-7.x

⬆️ 🛠️(deps): update dependency sphinx to v7.2.5 ([`230e211`](https://github.com/mraniki/talkytrend/commit/230e21154a9bd8b758eb42742f36ad5fcaba3c58))

### Update

* Update Requirements ([`7d5ea1f`](https://github.com/mraniki/talkytrend/commit/7d5ea1fe741b64725e335e137e4e2f4d1ebbc19a))

* :arrow_up: 🤖 Dependencies: Update sphinx requirement from 7.2.4 to 7.2.5

Updates the requirements on [sphinx](https://github.com/sphinx-doc/sphinx) to permit the latest version.
- [Release notes](https://github.com/sphinx-doc/sphinx/releases)
- [Changelog](https://github.com/sphinx-doc/sphinx/blob/master/CHANGES)
- [Commits](https://github.com/sphinx-doc/sphinx/compare/v7.2.4...v7.2.5)

---
updated-dependencies:
- dependency-name: sphinx
  dependency-type: direct:development
...

Signed-off-by: dependabot[bot] &lt;support@github.com&gt; ([`4b0ea36`](https://github.com/mraniki/talkytrend/commit/4b0ea36711f1df6ee4cd4cf550f33998154ed369))

* Update Requirements ([`5ca6d1a`](https://github.com/mraniki/talkytrend/commit/5ca6d1a4f3f541fad27cbec34994576ebc748936))

### ⬆️

* ⬆️ 🛠️(deps): update dependency sphinx to v7.2.5 ([`4c6dbbb`](https://github.com/mraniki/talkytrend/commit/4c6dbbbcc072e46ae64188213f24efae95b749b6))


## v1.14.9 (2023-08-30)

### Update

* Update README.md ([`3ec910b`](https://github.com/mraniki/talkytrend/commit/3ec910b885b44e7ba0677c203658b2f414938056))


## v1.14.8 (2023-08-28)

### Other

* Merge pull request #134 from mraniki/dev ([`cea0e7b`](https://github.com/mraniki/talkytrend/commit/cea0e7b52d628489683497da2cdf45fe83b2587a))

* Merge branch &#39;main&#39; into dev ([`cfccd8f`](https://github.com/mraniki/talkytrend/commit/cfccd8fb0dd542826761f8b8ecf29e5cfdcadbb4))


## v1.14.7 (2023-08-28)

### Other

* Merge branch &#39;main&#39; into dev ([`b68afc7`](https://github.com/mraniki/talkytrend/commit/b68afc75e26116d55d2d89404c5d09db8b7b3843))

* Merge pull request #132 from mraniki/renovate/ruff-0.x

⬆️ 🛠️(deps): update dependency ruff to &gt;=0.0.286,&lt;0.0.287 ([`9556beb`](https://github.com/mraniki/talkytrend/commit/9556bebb108489b67cafb7da35b7095f81274cbd))

* Merge pull request #130 from mraniki/renovate/sphinx-7.x

⬆️ 🛠️(deps): update dependency sphinx to v7.2.4 ([`f1ff82f`](https://github.com/mraniki/talkytrend/commit/f1ff82fb0c8e6b0006d4bc2651aa91bf4b7508d0))

### Update

* Update Requirements ([`f788adc`](https://github.com/mraniki/talkytrend/commit/f788adcb13b6944469d0a334826a200dac1046db))

* Update Requirements ([`8175e2e`](https://github.com/mraniki/talkytrend/commit/8175e2eb7699c15c47845293077ccfe86e834a5b))

### ⬆️

* ⬆️ 🛠️(deps): update dependency sphinx to v7.2.4 ([`6ec9c82`](https://github.com/mraniki/talkytrend/commit/6ec9c82def0b2aa1ade9886cfb1edbbd44b6e515))

* ⬆️ 🛠️(deps): update dependency ruff to &gt;=0.0.286,&lt;0.0.287 ([`bdd641e`](https://github.com/mraniki/talkytrend/commit/bdd641e938843bbcf8faa16a5589faa4c80b0ccb))

### 🐛

* 🐛 📝  extension error ([`3fc57ce`](https://github.com/mraniki/talkytrend/commit/3fc57ce6e3699aac0bec01caea1ec5f9a8a25efe))


## v1.14.6 (2023-08-24)

### Update

* Update pyproject.toml ([`476a63c`](https://github.com/mraniki/talkytrend/commit/476a63c48ee168ec11796cdd0d241dbcce35495d))


## v1.14.5 (2023-08-24)

### Other

* Merge pull request #131 from mraniki/dev

🔥 cleanup ([`da68322`](https://github.com/mraniki/talkytrend/commit/da683221f02f0922737f706a2d67d07f920c9923))

### Update

* Update GitHub Actions dependencies ([`bad020e`](https://github.com/mraniki/talkytrend/commit/bad020ed4065418e4722371876816131424079fc))

* Update dependabot.yml ([`590019d`](https://github.com/mraniki/talkytrend/commit/590019d01f1e72c9132e7092822171842f5c28d0))

* Update renovate.json ([`877b378`](https://github.com/mraniki/talkytrend/commit/877b3781a8be0100583967ec09b4f5d66d9fbe4e))

### 👷

* 👷 CI cleanup ([`1b47fbe`](https://github.com/mraniki/talkytrend/commit/1b47fbebeef1734be73ba04deb2650137cff0c5b))

### 🔥

* 🔥 cleanup ([`9eb771f`](https://github.com/mraniki/talkytrend/commit/9eb771f0c28b6fbd8e37e160d824f30736526b26))


## v1.14.4 (2023-08-24)

### Other

* Merge pull request #128 from mraniki/dev

⚡  Various small improvement ([`c607d48`](https://github.com/mraniki/talkytrend/commit/c607d48085b3dc7d50b93512c405c4017183af39))

### Update

* Update main.py for table trend data (no header) ([`cdc4f88`](https://github.com/mraniki/talkytrend/commit/cdc4f8859edae5d8a2642fd4fc78d57a3e56cca3))

* Update main.py for the ticker yfinance ([`7fdaa6b`](https://github.com/mraniki/talkytrend/commit/7fdaa6b761f598dad188c6ed3aab81c90ed125e8))

* Update pyproject.toml ([`02ba443`](https://github.com/mraniki/talkytrend/commit/02ba4432cfdcb061cb8de18fdf689d074df0704f))

* Update test_unit.py ([`c612aac`](https://github.com/mraniki/talkytrend/commit/c612aac281bca3ee2edd5f51d590e2c828641955))

* Update main.py ([`90b8854`](https://github.com/mraniki/talkytrend/commit/90b8854c272fc5fff5b4ccc255fb9be4a149b188))

### ⬆️

* ⬆️(deps): update dependency sphinx to v7.1.2 ([`3cf48e9`](https://github.com/mraniki/talkytrend/commit/3cf48e92c3fcb58b38100fe2ed1037c763c3e6e6))


## v1.14.3 (2023-08-21)

### Other

* Merge pull request #126 from mraniki/dev

👷‍♂️Flow.yml ([`e31c23f`](https://github.com/mraniki/talkytrend/commit/e31c23fb9be62acfb778df4af208ffc2c4bb71cc))

* Merge branch &#39;main&#39; into dev ([`c50c652`](https://github.com/mraniki/talkytrend/commit/c50c65232aaf7344b645beeadf3479a02a410f30))

### Update

* Updated GitHub Actions workflow file for publishing and inheriting secrets. ([`06bb787`](https://github.com/mraniki/talkytrend/commit/06bb787d1d9d952b5a975eee48f8e28ce6b2e511))


## v1.14.2 (2023-08-20)

### Other

* Merge branch &#39;main&#39; into dev ([`8b9f3b9`](https://github.com/mraniki/talkytrend/commit/8b9f3b9e315e2af2a5a83116ebfeb6729c4d9d55))

### Update

* Update renovate.json ([`573374c`](https://github.com/mraniki/talkytrend/commit/573374cbbbba7137be0244fb04465dd4de812a10))


## v1.14.1 (2023-08-20)

### :fire:

* :fire: ([`9284554`](https://github.com/mraniki/talkytrend/commit/9284554f5e843abcb93a515b241c7900662a66c7))

### Other

* Merge branch &#39;main&#39; into dev ([`f557a20`](https://github.com/mraniki/talkytrend/commit/f557a200a0e0b9349bf0afe1cf47fdbec89c93d9))

* Merge branch &#39;main&#39; into dev ([`b7573f8`](https://github.com/mraniki/talkytrend/commit/b7573f8fc23695a59b0c4f3c1a40ebeb772c1fb0))

### Update

* Update pyproject.toml ([`8efbce1`](https://github.com/mraniki/talkytrend/commit/8efbce10db683ca82722d1cf8026f26201ad4066))

### ⬆️

* ⬆️(deps): update dependency sphinx to v7.2.2 ([`17490ad`](https://github.com/mraniki/talkytrend/commit/17490ad2d8119fda982ff26bcf7163fe1f46bdc9))


## v1.14.0 (2023-08-18)

### :arrow_up:

* :arrow_up: ([`7725803`](https://github.com/mraniki/talkytrend/commit/772580346421a56a7198207aed9b4b2fd005692d))

### :bug:

* :bug: settings name ([`be9dff8`](https://github.com/mraniki/talkytrend/commit/be9dff841c9a3bf0a7ca1bb1c0bb97cbe7686dd1))

* :bug: var name ([`01e8f5d`](https://github.com/mraniki/talkytrend/commit/01e8f5dee93a6bb0a751be44e348a1ec4c4e0e00))

### :rotating_light:

* :rotating_light: ([`67a2bfb`](https://github.com/mraniki/talkytrend/commit/67a2bfb4c52393ecaf513463982e47f74348cfc1))

### :wrench:

* :wrench: help message ([`922a371`](https://github.com/mraniki/talkytrend/commit/922a3712976728337b64178fec960a4b84796948))

* :wrench: help message ([`a04bc4e`](https://github.com/mraniki/talkytrend/commit/a04bc4e027f855a5b38e198284e5096cb722d1df))

* :wrench:jackson hall meeting ([`5892b34`](https://github.com/mraniki/talkytrend/commit/5892b34fbe157e3b626a39e7de859bd95683927c))

### Feat

* :sparkles: feat: Add dependencies and settings for SERP API ([`81f5e05`](https://github.com/mraniki/talkytrend/commit/81f5e054635cd73221c66df57d48ed432e989b0b))

### Other

* Merge pull request #122 from mraniki/dev ([`c9bda24`](https://github.com/mraniki/talkytrend/commit/c9bda24d85f1948e374346dec2819778b2122976))

* Merge branch &#39;dev&#39; of git@github.com:mraniki/talkytrend.git ([`6aeece9`](https://github.com/mraniki/talkytrend/commit/6aeece94745addab42bbe2f3a4fe2669034676de))

### Update

* Update Requirements ([`cf33e05`](https://github.com/mraniki/talkytrend/commit/cf33e05ea1320e5803007346d5547838775335cf))

### ⚡

* ⚡ adding indicator ([`6e8918a`](https://github.com/mraniki/talkytrend/commit/6e8918acf56ae497d3037533a7cda0853829cc31))


## v1.13.4 (2023-08-18)

### :recycle:

* :recycle::arrow_up:dep downgrade ([`854c549`](https://github.com/mraniki/talkytrend/commit/854c5499b982a357f177011190ece1d5acb4c075))

### Other

* Merge pull request #118 from mraniki/dev

:recycle::arrow_up:dep downgrade ([`a5215e4`](https://github.com/mraniki/talkytrend/commit/a5215e40ceac0e00a029371964006b9c8a9f25ec))

### Update

* Update Requirements ([`4661887`](https://github.com/mraniki/talkytrend/commit/46618878ddea5ef7aa95d2defdde13df3419a832))


## v1.13.3 (2023-08-12)

### :memo:

* :memo: ([`b3000ef`](https://github.com/mraniki/talkytrend/commit/b3000ef1c4c6c16f59f52f4e43282d734abf5db9))

### Other

* Merge pull request #117 from mraniki/dev

📝 Documentation update ([`783a98a`](https://github.com/mraniki/talkytrend/commit/783a98a49632b383308015ebbac2ae2c4cb849c9))

### 🐛

* 🐛  logo update ([`0ac0a7c`](https://github.com/mraniki/talkytrend/commit/0ac0a7cdd82a06283cd3308e9eca1e22f37e889d))


## v1.13.2 (2023-08-11)

### :wrench:

* :memo: :wrench:  settings documentation ([`f3228b9`](https://github.com/mraniki/talkytrend/commit/f3228b9031bca94bf03c61b594726ecc371b98ea))

### Other

* Merge pull request #116 from mraniki/dev ([`742a4f7`](https://github.com/mraniki/talkytrend/commit/742a4f700fdaa2824d1be6f7dfa4bd41b04c252f))

### Update

* Update Requirements ([`699cab4`](https://github.com/mraniki/talkytrend/commit/699cab43296d1faed43cf1f95f8b72e9f63c19bd))

### 👷

* 👷 CI Change ([`b98bca6`](https://github.com/mraniki/talkytrend/commit/b98bca6725c2681f4c8c3a77cbe42ee9cfb08c7b))

### 🔊

* 🔊 added better debug logging ([`4a706ad`](https://github.com/mraniki/talkytrend/commit/4a706adae03732e6299284f124c5f99a95757884))


## v1.13.1 (2023-08-09)

### Other

* Merge pull request #107 from mraniki/renovate/ruff-0.x

⬆️(deps): update dependency ruff to &gt;=0.0.283,&lt;0.0.284 ([`2597709`](https://github.com/mraniki/talkytrend/commit/25977091a89ec7f2ea782d035c65b6ef550d4b43))

### ⬆️

* ⬆️(deps): update dependency ruff to &gt;=0.0.283,&lt;0.0.284 ([`fac7b54`](https://github.com/mraniki/talkytrend/commit/fac7b54805d9feca06c34b743db825c7a1ef56e2))


## v1.13.0 (2023-08-08)

### :memo:

* :memo: ([`fb80ffe`](https://github.com/mraniki/talkytrend/commit/fb80ffee73aecc37fb2289f68f06250f6cc336c0))

### Other

* Merge pull request #113 from mraniki/dev

📝 🔧 settings documentation ([`775dcd0`](https://github.com/mraniki/talkytrend/commit/775dcd020848c410fb5c51e2f76fcce537437f1c))

### ✨

* ✨ added yfinance ticker as part of the monitor ([`2ff386c`](https://github.com/mraniki/talkytrend/commit/2ff386c5f54d9fdca8a6720e5bb9b4d62793110e))

### 📝

* 📝 conf update for intersphinx ([`3054aab`](https://github.com/mraniki/talkytrend/commit/3054aaba851ef4c35ac4d5bd51a5aded290d5f1a))

### 🔧

* 📝 🔧  settings documentation ([`7cf7612`](https://github.com/mraniki/talkytrend/commit/7cf76125004232901b38ddfbe5c953699fd1bcbc))


## v1.12.11 (2023-08-07)

### Other

* Merge pull request #112 from mraniki/dev

💬 readme update ([`1dce943`](https://github.com/mraniki/talkytrend/commit/1dce943f8fcddbb9194cb96d80b53e3608a9c6ff))

### 💬

* 💬 readme update ([`314424f`](https://github.com/mraniki/talkytrend/commit/314424fbebad5cf137fe1cb89764d66d376bf351))


## v1.12.10 (2023-08-07)

### Fix

* 📝 intersphinx fix ([`21ba799`](https://github.com/mraniki/talkytrend/commit/21ba79902de2880e037e87ad3c1af8617a6d0108))

* 📝  fix ([`a8ba095`](https://github.com/mraniki/talkytrend/commit/a8ba095db6d7175aa4c9cf8f7d82e996d04da06e))

### Other

* Merge pull request #111 from mraniki/dev

📝  sphinx.ext.extlinks ([`76f4a3f`](https://github.com/mraniki/talkytrend/commit/76f4a3f80a1a8e4b052e8d61c03f6e1fb4ca6182))

### 🎨

* 🎨 improve docs with hovering ([`beeee54`](https://github.com/mraniki/talkytrend/commit/beeee54fab34f8c3eb1e10dbe5f537995e4f178e))

### 📝

* 📝 updating docs template and sphinx ([`bfeaac0`](https://github.com/mraniki/talkytrend/commit/bfeaac09366c57551e8ad33532ffa5ff21827e2e))

* 📝  sphinx.ext.extlinks ([`243ef4b`](https://github.com/mraniki/talkytrend/commit/243ef4b9acdca3aaa6e649e265015a94d4fc7077))


## v1.12.9 (2023-08-06)

### Other

* Merge pull request #110 from mraniki/dev

📝 intersphinx setup ([`cd68c2e`](https://github.com/mraniki/talkytrend/commit/cd68c2eecefcf05fdb2102de27a639a54acd3650))

* Merge branch &#39;main&#39; into dev ([`dbff80c`](https://github.com/mraniki/talkytrend/commit/dbff80c3c9f16ebe49546912880530f60666a409))

* Delete IMG_3091.png ([`e7e12d8`](https://github.com/mraniki/talkytrend/commit/e7e12d80b00def1c2812d7ee01ab905a76724f6a))

### 📝

* 📝 intersphinx setup ([`8c9fb4d`](https://github.com/mraniki/talkytrend/commit/8c9fb4d3b1ced6de6393f68342e41fc89b7a4dbe))

### 🧐

* 🧐 ([`fc521f8`](https://github.com/mraniki/talkytrend/commit/fc521f861a947fc502d6764a4f43a8fd768b5fbb))


## v1.12.8 (2023-08-06)

### Other

* Delete logo-full.png ([`4cd999f`](https://github.com/mraniki/talkytrend/commit/4cd999fc5054395c3c08e7cb1544d4aa4df5ae3e))

* Add files via upload ([`d0448e8`](https://github.com/mraniki/talkytrend/commit/d0448e89a4f2cca13a87d3337a31f3b24dc93d2c))

* Merge branch &#39;main&#39; into dev ([`1919763`](https://github.com/mraniki/talkytrend/commit/1919763eb46267b3917ed72db28d213be9892c89))

### Update

* Update README.md ([`4feb165`](https://github.com/mraniki/talkytrend/commit/4feb1652386bacdb07cfb76424acaefc869075ec))


## v1.12.7 (2023-08-05)

### Update

* Update README.md ([`85f4706`](https://github.com/mraniki/talkytrend/commit/85f4706ae4069620f3726cb33a90a4aec8bae977))

### 🎨

* 🎨 ([`20cfe0e`](https://github.com/mraniki/talkytrend/commit/20cfe0eb94ea411a0bcf3c2ec00433c6c6106a5c))


## v1.12.6 (2023-08-04)

### :bug:

* :memo::bug: ([`a380012`](https://github.com/mraniki/talkytrend/commit/a38001260bffe66b29adb5fde4d91a18af4b7b3c))

### Other

* Merge pull request #108 from mraniki/dev

:memo::bug: ([`dc4898b`](https://github.com/mraniki/talkytrend/commit/dc4898b8b81ea7ca105241da4bc22740dc54826b))

### Update

* Update Requirements ([`1e505c2`](https://github.com/mraniki/talkytrend/commit/1e505c216c741aa76f7da211d3e8a9034c2044e1))

### ✅

* ✅ Unit Test ([`d35b85b`](https://github.com/mraniki/talkytrend/commit/d35b85b6d0ed3c1dbf03994d816f23d6d49ac5a6))

### 📝

* 📝 ([`dfd3a7a`](https://github.com/mraniki/talkytrend/commit/dfd3a7a0521c7239904f3625e5fa78c131a300b1))

* 📝 RTD update ([`6649ae2`](https://github.com/mraniki/talkytrend/commit/6649ae20a12905c874c0311f4be910a08fba843e))


## v1.12.5 (2023-08-03)

### Other

* Merge pull request #104 from mraniki/dependabot/pip/ruff-gte-0.0.280-and-lt-0.0.283

⬆️ Dep: Update ruff requirement from ^0.0.280 to &gt;=0.0.280,&lt;0.0.283 ([`51ab0a1`](https://github.com/mraniki/talkytrend/commit/51ab0a1bc50d7d92aac63e39b680371697ba4370))


## v1.12.4 (2023-08-03)

### Other

* Merge pull request #105 from mraniki/dev

📝 RTD initial release ([`d4968e7`](https://github.com/mraniki/talkytrend/commit/d4968e7a15dde26f19f069e46b56bf7c62d8a9f9))

### Update

* Update Requirements ([`4474f6f`](https://github.com/mraniki/talkytrend/commit/4474f6f7598998a0c4032019a210ba834e81754b))

* Update Requirements ([`5c87328`](https://github.com/mraniki/talkytrend/commit/5c87328453bfd37a972d6ebaf87a0bf37250c79a))

* :arrow_up: Dep: Update ruff requirement

Updates the requirements on [ruff](https://github.com/astral-sh/ruff) to permit the latest version.
- [Release notes](https://github.com/astral-sh/ruff/releases)
- [Changelog](https://github.com/astral-sh/ruff/blob/main/BREAKING_CHANGES.md)
- [Commits](https://github.com/astral-sh/ruff/compare/v0.0.280...v0.0.282)

---
updated-dependencies:
- dependency-name: ruff
  dependency-type: direct:development
...

Signed-off-by: dependabot[bot] &lt;support@github.com&gt; ([`f4b9c77`](https://github.com/mraniki/talkytrend/commit/f4b9c7728e69d631226ef8865ae64caca75c8624))

### 📝

* 📝 ([`06652de`](https://github.com/mraniki/talkytrend/commit/06652de3b96d4921eb61eb9f5343fef6d958320a))

* 📝 RTD ([`48db4ba`](https://github.com/mraniki/talkytrend/commit/48db4ba8012027b37d85822d45fa5e3d97fa85ec))

* 📝 RTD update ([`7b95cc0`](https://github.com/mraniki/talkytrend/commit/7b95cc0f537d94b07995f1451b322e52499e9ee6))

* 📝 RTD initial release ([`4b3d239`](https://github.com/mraniki/talkytrend/commit/4b3d2390f8f63fbbac3befe26e5a6e08de15cc6b))


## v1.12.3 (2023-07-26)

### Other

* Merge pull request #102 from mraniki/dev

✅ Unit Test ([`006f16b`](https://github.com/mraniki/talkytrend/commit/006f16b1b0403bbcd8e650722ae07d46da067c1e))

* Merge pull request #101 from mraniki/sourcery/dev

:zap: monitor output tweak (Sourcery refactored) ([`11b6a66`](https://github.com/mraniki/talkytrend/commit/11b6a66226a276443d9b8d790ff6bebe752d3f93))

### ✅

* ✅ Unit Test ([`f49483d`](https://github.com/mraniki/talkytrend/commit/f49483de36b0b36b42da85faaf2578f3a34addd2))

* ✅ Unit Test ([`dde5d23`](https://github.com/mraniki/talkytrend/commit/dde5d2353222e4ccdad5109f454a67ecfc5000ac))

* ✅ Unit Test ([`4b953aa`](https://github.com/mraniki/talkytrend/commit/4b953aa50de988c10d336ca3d9fb393d1610d439))


## v1.12.2 (2023-07-26)

### :zap:

* :zap: monitor output tweak ([`bdc86a9`](https://github.com/mraniki/talkytrend/commit/bdc86a96974ac0188d04c99a6261cbc20be9d2d1))

### Other

* &#39;Refactored by Sourcery&#39; ([`0ab751a`](https://github.com/mraniki/talkytrend/commit/0ab751a99b13e64ba9dde02d685f5e5a09c0fce8))

* Merge pull request #100 from mraniki/dev

:zap: monitor output tweak ([`9e09ae4`](https://github.com/mraniki/talkytrend/commit/9e09ae430030809673ab7bf28ea3bb601f733934))


## v1.12.1 (2023-07-25)

### Other

* Merge pull request #99 from mraniki/dev

✅ test_fetch_instrument_info ([`6037226`](https://github.com/mraniki/talkytrend/commit/6037226063e657212b758a85760489cbb4290fa0))

### ✅

* ✅ ([`770af84`](https://github.com/mraniki/talkytrend/commit/770af84502d4cf81a2f99774ca87770955d019ae))

* ✅ test_fetch_instrument_info ([`06b0633`](https://github.com/mraniki/talkytrend/commit/06b063395a320ebd14a8e8a3716850acc9f005f4))


## v1.12.0 (2023-07-25)

### Other

* Merge pull request #98 from mraniki/dev

♻️✅ remove scanner, added monitor now ([`cc7f2d4`](https://github.com/mraniki/talkytrend/commit/cc7f2d4b586cfe193db8b69aa1a4fd21c6f725ff))

### ✅

* ✅ ([`e792dbf`](https://github.com/mraniki/talkytrend/commit/e792dbf343732b5ec049112dd56260580e0119d1))

* ✅ ([`478bd9c`](https://github.com/mraniki/talkytrend/commit/478bd9c8dfa6b655782738ac84069ac801c13ee5))

* ✅ updated monitor unit test ([`94fd15c`](https://github.com/mraniki/talkytrend/commit/94fd15cef7416eac91a0f463047dd09b6bacc87b))

* ✅ unit test for monitor ([`a90bab1`](https://github.com/mraniki/talkytrend/commit/a90bab195b2fd401cf091b341c06bc69bbf3d4d1))

* ✅ loguru pytest ([`94724cb`](https://github.com/mraniki/talkytrend/commit/94724cbd18453045717bb5376679d89b4fefc4c1))

### ✨

* ✨ deprecate scanner in favor of monitor. ([`154ea12`](https://github.com/mraniki/talkytrend/commit/154ea12f5785586a8416b35a9f86a1c84d614c7e))

### 🔧

* 🔧 updated config and example ([`23ebbc3`](https://github.com/mraniki/talkytrend/commit/23ebbc3bb430a7efba2a6c38a6ad8bec0dd9e387))

### 🚨

* 🚨 ([`ae708b8`](https://github.com/mraniki/talkytrend/commit/ae708b8c6be64b7dd444b404540eda215a4e8fd9))


## v1.11.0 (2023-07-24)

### Other

* Merge pull request #94 from mraniki/dev

⚗️ experimenting yfinance ([`465ec23`](https://github.com/mraniki/talkytrend/commit/465ec237e277dd5c671e4bdd1e6c0c41a3e35f5d))

* Merge branch &#39;main&#39; into dev ([`2e7cdeb`](https://github.com/mraniki/talkytrend/commit/2e7cdeb18d8195d23fc240e5edde457dee472c81))

### Update

* Update Requirements ([`93e73be`](https://github.com/mraniki/talkytrend/commit/93e73be3d484c3f6b17ff4b1ab91af894d980519))

* Update Requirements ([`7cda2ef`](https://github.com/mraniki/talkytrend/commit/7cda2efa45a78e19677e5e8b40a972a415fa5d2c))

### 🔥

* 🔥  remove news until stabilize ([`b1b4fad`](https://github.com/mraniki/talkytrend/commit/b1b4fad27be0f58d3db3725ba2089d6b86415177))

### 🥚

* 🥚 🔊 loguru implementation ([`d3e8c7d`](https://github.com/mraniki/talkytrend/commit/d3e8c7db8429c926df41e0d4b150c8f57241c8cf))


## v1.10.1 (2023-07-22)

### Other

* Merge pull request #95 from mraniki/sourcery/dev

⚗️ experimenting yfinance (Sourcery refactored) ([`27cc642`](https://github.com/mraniki/talkytrend/commit/27cc6427569f8fb457b4fa4eded3c9c06e88be73))

* &#39;Refactored by Sourcery&#39; ([`dece064`](https://github.com/mraniki/talkytrend/commit/dece064070bfd587984181280842dde9f8cac468))

### Update

* Update pyproject.toml ([`9900de5`](https://github.com/mraniki/talkytrend/commit/9900de5e5566d38ff02de79c6bc4d07a24e22e55))

* Update Requirements ([`147eac4`](https://github.com/mraniki/talkytrend/commit/147eac4196675b602fc4da4ef82e89111081e25b))

### ⚗️

* ⚗️ experimenting yfinance ([`e157bf7`](https://github.com/mraniki/talkytrend/commit/e157bf717ac66fe245a8441d115a669fdfe00c65))

### 🔧

* 🔧 cleanup ([`54aa9e2`](https://github.com/mraniki/talkytrend/commit/54aa9e2bf31f3c97442c89edd8c11505b79c152e))

### 🚨

* 🚨 ([`060e8e2`](https://github.com/mraniki/talkytrend/commit/060e8e2b1df2b7f29b57d51e14d8592e3ebabcd0))


## v1.10.0 (2023-07-20)

### Other

* Merge pull request #93 from mraniki/dev

🔧  config alignment ([`6f84999`](https://github.com/mraniki/talkytrend/commit/6f849991895b005a1b4e5b22eca68c7b965f1b22))

### ✅

* ✅ unit test update ([`9c30cc7`](https://github.com/mraniki/talkytrend/commit/9c30cc7c7a9200b9f02be9a6fd8c5285034dd2e8))

### 🔧

* 🔧  config alignment ([`312c754`](https://github.com/mraniki/talkytrend/commit/312c754ae5b6072cf028bb2b8ff98875bdb40d5d))

### 🥚

* 🥚 adding command add module level ([`018f023`](https://github.com/mraniki/talkytrend/commit/018f023654af314732f03ac88642e35e7a928996))


## v1.9.2 (2023-07-19)

### Other

* Merge pull request #92 from mraniki/dev

Update Requirements ([`ace27b5`](https://github.com/mraniki/talkytrend/commit/ace27b5c0a1ef1d61e5ebbc45f71b5cb1a25f327))

* Merge branch &#39;main&#39; into dev ([`5e26f2d`](https://github.com/mraniki/talkytrend/commit/5e26f2d84068ec73bb73c0194d3c72e726c4ddd6))

* Merge branch &#39;main&#39; into dev ([`e36fbb6`](https://github.com/mraniki/talkytrend/commit/e36fbb613a99a5a28b8b5cb24c93b5041818ac5a))

* Merge pull request #91 from mraniki/dev

👷 CI Change test ([`64b95f9`](https://github.com/mraniki/talkytrend/commit/64b95f9ce6e859c439397d9b98b80fecaa218c2c))

* Merge pull request #90 from mraniki/dev

👷 CI Change testing CI update ([`ead25f3`](https://github.com/mraniki/talkytrend/commit/ead25f31ef56f4f25e2f0c10980213399e5d64bf))

### Update

* Update Requirements ([`d10f5e5`](https://github.com/mraniki/talkytrend/commit/d10f5e560012b10faa6476b3d3c697982ac40cb3))

* Update Requirements ([`7355f77`](https://github.com/mraniki/talkytrend/commit/7355f77e7f98cd5bf7a84a9d544753288e0dacd3))

* Update Requirements ([`7e94c8c`](https://github.com/mraniki/talkytrend/commit/7e94c8c6fe2768bb949bd6499ee74efcc059c397))

* Update Requirements ([`2bee78a`](https://github.com/mraniki/talkytrend/commit/2bee78a916e12cf164621f4ac611c3c5e75cfa5e))

### 👷

* 👷 CI Change test ([`d2ec946`](https://github.com/mraniki/talkytrend/commit/d2ec94663e86eca799c8f7eb9e44d8980157ec6d))

* 👷 CI Change testing CI update ([`8ab2d3f`](https://github.com/mraniki/talkytrend/commit/8ab2d3f1b46874e916065e3e8ea2edd3ab31f2c8))


## v1.9.1 (2023-07-19)

### Other

* Merge pull request #89 from mraniki/dev

⚡ enhance the event filtering to filter out past event of the day ([`99ff07e`](https://github.com/mraniki/talkytrend/commit/99ff07eeba12f22811d04caf3e9461aacd941f01))

* Merge pull request #88 from mraniki/dev

👷 CI Change for semantic ([`4fc205d`](https://github.com/mraniki/talkytrend/commit/4fc205d7fb7a6fbe53242dc3d9f426fb8c66752f))

* Merge pull request #86 from mraniki/dev

⬆️ renovate exclude req ([`a32a5a6`](https://github.com/mraniki/talkytrend/commit/a32a5a65531050ff791fc18a719b41e5322ed702))

* Merge pull request #84 from mraniki/renovate/pydantic-2.x

⬆️(deps): update dependency pydantic to v2 ([`2ffbbdb`](https://github.com/mraniki/talkytrend/commit/2ffbbdb8d7f460583cb2807766cc05abf38d0066))

* Merge pull request #83 from mraniki/dev

👷 CI Change in pytoml + ruff + semantic ([`a61c768`](https://github.com/mraniki/talkytrend/commit/a61c768c21f1faacf78c938a29e31b38dacc131b))

### Update

* Update Requirements ([`e2f4a92`](https://github.com/mraniki/talkytrend/commit/e2f4a92e7f8663eb5673e6e4d6fc08582ec054ad))

* Update Requirements ([`6fcb3ed`](https://github.com/mraniki/talkytrend/commit/6fcb3eda06a063b4ae6948628ae163175dbe21c2))

* Update Requirements ([`6fbf92e`](https://github.com/mraniki/talkytrend/commit/6fbf92ee867533111b6d0c337d517b5d99e573ff))

* Update Requirements ([`5e6c8e5`](https://github.com/mraniki/talkytrend/commit/5e6c8e583b98dcb67b1ee18fa8079b1e5ab75989))

### ⚗️

* ⚗️ ([`8cbe9e9`](https://github.com/mraniki/talkytrend/commit/8cbe9e9f06115a7fcc30c3ebe656a7f25907d7d5))

### ⚡

* ⚡ enhance the event filtering to filter out past event of the day ([`a8fbce1`](https://github.com/mraniki/talkytrend/commit/a8fbce17778e16ff64f9c6bc99e7c36fad0dfa70))

### ⬆️

* ⬆️ renovate exclude req ([`b312a39`](https://github.com/mraniki/talkytrend/commit/b312a3974713a2b0214b1fe742215ce6ba1d48b4))

* ⬆️(deps): update dependency pydantic to v2 ([`02ab5e5`](https://github.com/mraniki/talkytrend/commit/02ab5e59bc549f19400419e324ead879224e2ac2))

* ⬆️ 👷 CI Change ([`b8f6326`](https://github.com/mraniki/talkytrend/commit/b8f632631651a414ff98946b1ba55300a3c7715a))

* ⬆️ 👷 CI Change and Semantic ([`71a47cb`](https://github.com/mraniki/talkytrend/commit/71a47cbc6268e0d769cf4604f193402865581902))

### 🎨

* 🎨 ([`bfd6b23`](https://github.com/mraniki/talkytrend/commit/bfd6b23b7ff9a06b59507bdb5decb0a03715ce96))

* 🎨  settings ([`77caa65`](https://github.com/mraniki/talkytrend/commit/77caa65be023369ee81682c8cb4a9d6abd22f9cb))

### 👷

* 👷 CI Change for semantic ([`73b91e1`](https://github.com/mraniki/talkytrend/commit/73b91e1663b7357a085f0a06a6be6c8ec35dee00))

* Rename 👷‍♂️Flow.yml to 👷Flow.yml ([`d7f89ad`](https://github.com/mraniki/talkytrend/commit/d7f89adc41d5e08fa5101a06b81bb7e2f5b2f6ba))

* 👷 CI Change in pytoml + ruff ([`3032e6c`](https://github.com/mraniki/talkytrend/commit/3032e6c9828d2e0807b130e1f218b065cb877a33))

### 🔥

* 🔥 asset_signals removed ([`5fc6dc6`](https://github.com/mraniki/talkytrend/commit/5fc6dc6fa70d89d8440ef0ef7c6afbd71334f19c))

### 🚨

* 🚨 linter sorting ([`852d69a`](https://github.com/mraniki/talkytrend/commit/852d69aef99902ed84d7a92030818d1ca2d5724c))

* 🚨 linter sorting ([`006d991`](https://github.com/mraniki/talkytrend/commit/006d9918f120e2d3093c9e9cc6976cbcf46504c7))

* 🚨 linter config ([`78e62e4`](https://github.com/mraniki/talkytrend/commit/78e62e484d9d4e8631e7d5fb9a7fab3fa9656f34))


## v1.9.0 (2023-07-17)

### :wrench:

* :wrench: cleanup ([`ac959c2`](https://github.com/mraniki/talkytrend/commit/ac959c228deb1dfaab4912c46f3f515ca9a56c13))

### Other

* Merge pull request #82 from mraniki/dev

🔧 feed config update ([`b363075`](https://github.com/mraniki/talkytrend/commit/b36307549288f47353721f10cea5308c2a55fe75))

### ⚡

* ⚡ simplify the check signal ([`8f99455`](https://github.com/mraniki/talkytrend/commit/8f99455f02dfee776e28113fb67ca017e9031296))

* ⚡ warning instead of error ([`5141295`](https://github.com/mraniki/talkytrend/commit/514129507cc6e4c08439c557ae95e0243a085a16))

### ✨

* ✨ updated check_signal function  and 🚨 updated ruff ([`e2b6899`](https://github.com/mraniki/talkytrend/commit/e2b68992624615847c6e79f8f941bb47b161fd6a))

### 🔧

* 🔧 cleanup ([`7492e14`](https://github.com/mraniki/talkytrend/commit/7492e14eb0223e64a5ed7cc2739c077f59a9382b))

* 🔧 updating config ([`9acf690`](https://github.com/mraniki/talkytrend/commit/9acf690df8b53f1a8d84db909b247c519bb7fa8a))

* 🔧 feed config update ([`1f579e2`](https://github.com/mraniki/talkytrend/commit/1f579e29e3572853a1951a70dc4e5460d3066cf8))


## v1.8.3 (2023-07-17)

### Other

* Merge pull request #81 from mraniki/renovate/python-semantic-release-8.x

⬆️(deps): update dependency python-semantic-release to v8 ([`b0bbad4`](https://github.com/mraniki/talkytrend/commit/b0bbad49ef39a4e8c2f1b4776dcaad65881ea946))


## v1.8.2 (2023-07-17)

### Other

* Merge pull request #80 from mraniki/dev

🐛 error handling in case feed is not valid xml ([`be9f057`](https://github.com/mraniki/talkytrend/commit/be9f057b4cb55f959b12ed9077590e1292ae5fc5))

### ⬆️

* ⬆️(deps): update dependency python-semantic-release to v8 ([`ea8dac5`](https://github.com/mraniki/talkytrend/commit/ea8dac5cb04d0e8bd750ddaa5ad0a85276971d2a))

### 🐛

* 🐛  error handling in case feed is not valid xml ([`c616e36`](https://github.com/mraniki/talkytrend/commit/c616e36f2b179c5016c074b3aff2141db69e0443))

### 🥅

* 🥅 incorrect xml ([`056b996`](https://github.com/mraniki/talkytrend/commit/056b996a82e2f56e5117ba99271a0c9ece3f27e6))


## v1.8.1 (2023-07-15)

### Other

* Merge pull request #79 from mraniki/dev

🐛 info help function ([`4fd2e70`](https://github.com/mraniki/talkytrend/commit/4fd2e707cf9459941e64d647e2619bcb6ee9ffc5))

### 🐛

* 🐛 info help function ([`c4f7713`](https://github.com/mraniki/talkytrend/commit/c4f7713269bc3ed665cd9af2d4a21ddde0f5080e))


## v1.8.0 (2023-07-15)

### :art:

* :art: help message ([`e817668`](https://github.com/mraniki/talkytrend/commit/e81766810746df80ed7b4318019708d3f3ae1b8c))

### Other

* Merge pull request #78 from mraniki/dev

✨ adding get_tv function ([`26df11b`](https://github.com/mraniki/talkytrend/commit/26df11b372e6e654919afbb79c1d9307b934cd03))

### Update

* Update main.py ([`8fbe860`](https://github.com/mraniki/talkytrend/commit/8fbe8602be48be6f809ab27ce0a164ba439d0f59))

* Update test_unit.py ([`9601ce8`](https://github.com/mraniki/talkytrend/commit/9601ce8a3347a294008718cdd53f10cf2825bec7))

* Update main.py ([`9683b03`](https://github.com/mraniki/talkytrend/commit/9683b03f4a9da4c01b4fee9f193707c6d4024e54))

### ✨

* ✨ adding get_tv function ([`eb0840e`](https://github.com/mraniki/talkytrend/commit/eb0840e3c3d751d24b96266e6188a698f9122fa2))


## v1.7.4 (2023-07-13)

### :wrench:

* :wrench: Financialjuice feed ([`98a6077`](https://github.com/mraniki/talkytrend/commit/98a60775a5ae314b6267d4a9f83a6c346205009b))

### Other

* Merge pull request #77 from mraniki/dev ([`8f3d043`](https://github.com/mraniki/talkytrend/commit/8f3d043aa18406d4e4208d6c1f5cc93bfe0a59c2))


## v1.7.3 (2023-07-12)

### Fix

* 🚑 check signal _hotfix ([`084582d`](https://github.com/mraniki/talkytrend/commit/084582deaa8d1890e7eb932d09b88eed836e23dc))

### Other

* Merge pull request #74 from mraniki/dev

🚑 check signal _hotfix ([`0d818c4`](https://github.com/mraniki/talkytrend/commit/0d818c427aaa051783bdcf28fd5ac1c37ccd818f))

* Merge pull request #75 from mraniki/sourcery/dev

🚑 check signal _hotfix (Sourcery refactored) ([`9de9187`](https://github.com/mraniki/talkytrend/commit/9de9187f48238cf026dfb8945bb453f08bbe7121))

* &#39;Refactored by Sourcery&#39; ([`1ce1f56`](https://github.com/mraniki/talkytrend/commit/1ce1f56707dc33e4c30034b0379813f6ed26d4fa))

### ✅

* ✅ improve unit test ([`f025c49`](https://github.com/mraniki/talkytrend/commit/f025c4911e0e238b612d0f7ce36ca31ea8d8b2ed))


## v1.7.2 (2023-07-11)

### Other

* Merge pull request #72 from mraniki/dev

⚡ improving for plugin capability ([`f25626b`](https://github.com/mraniki/talkytrend/commit/f25626b75b8e7b1f30fbb05e01f661c5ee3b1928))

* Merge pull request #73 from mraniki/sourcery/dev

⚡ improving for plugin capability (Sourcery refactored) ([`79b861d`](https://github.com/mraniki/talkytrend/commit/79b861d9ae50e57131c2b0718b9c8b82cc7cc47a))

* &#39;Refactored by Sourcery&#39; ([`e50fecf`](https://github.com/mraniki/talkytrend/commit/e50fecfb10353eb63702321307c0a7a4c2a22e58))

### ⚡

* ⚡ improving for plugin capability ([`fb86d2b`](https://github.com/mraniki/talkytrend/commit/fb86d2b90dd5024a74bb29537bafcf3e2a83914d))


## v1.7.1 (2023-07-10)

### Other

* Merge pull request #71 from mraniki/dev

🚑 removing HTML for signal trend due to chat limitation ([`88b5e36`](https://github.com/mraniki/talkytrend/commit/88b5e3675f14adaedca46329d9ae3cf475de83f7))

### 🚑

* 🚑 removing HTML for signal trend due to chat limitation ([`b2f741b`](https://github.com/mraniki/talkytrend/commit/b2f741baa08ca88a286d630c12f4cf15840b3bd1))


## v1.7.0 (2023-07-10)

### :memo:

* :memo: pyproject doc related update ([`a6ced18`](https://github.com/mraniki/talkytrend/commit/a6ced18ac8cba70f7fc12e962585bef748723d94))

* :memo: readmydocs updated config ([`84035c9`](https://github.com/mraniki/talkytrend/commit/84035c9ae64942119b2c27b7921811ba79d1ec3b))

### :rotating_light:

* :rotating_light: linter ([`afa8591`](https://github.com/mraniki/talkytrend/commit/afa8591c3427ffa68a7bdcdb69b670117b5313b0))

* :rotating_light: linter ([`20642eb`](https://github.com/mraniki/talkytrend/commit/20642ebb13392b918091a190594f1ca09c14e23b))

### :sparkles:

* :sparkles: Added  stop_scanning method and test_unit.py ([`32f97b6`](https://github.com/mraniki/talkytrend/commit/32f97b6e92ec87523597d17dfd99b6c79d4c836f))

### :white_check_mark:

* :white_check_mark:  unit test for scanner ([`7c2ba5b`](https://github.com/mraniki/talkytrend/commit/7c2ba5b866dbc339010d8e47fc23cd993473f0f8))

### Fix

* 🐛 updating the scanning freq in the try except and fix the test ([`d066b83`](https://github.com/mraniki/talkytrend/commit/d066b83e2f084a010a643e3fef8899c61913cc35))

* 🔒 fix the xml security issue by parsing with dict instead. ([`60fb19a`](https://github.com/mraniki/talkytrend/commit/60fb19a5c3d22ee262907c211a3cf89355448590))

### Other

* Merge pull request #66 from mraniki/dev

✨ 💄 Delivery of the trading view trend as html table + feeds news ([`6d81cb4`](https://github.com/mraniki/talkytrend/commit/6d81cb458e25ed19064326d4fec246ca5f7726ac))

* Merge branch &#39;main&#39; into dev ([`e99c05e`](https://github.com/mraniki/talkytrend/commit/e99c05e2c4fbba9eaaf4731d3e8cac5053a3475e))

* Delete poetry.lock ([`5def6c6`](https://github.com/mraniki/talkytrend/commit/5def6c608eca1b51f7fe96f85a6ea405c7e157a5))

* :private: defusedxml ([`8cb807b`](https://github.com/mraniki/talkytrend/commit/8cb807bffca8167641e66c3fa536068665a8d272))

* Merge pull request #67 from mraniki/sourcery/dev

✨ 💄 Delivery of the trading view trend as html table + feeds news (Sourcery refactored) ([`6aaada6`](https://github.com/mraniki/talkytrend/commit/6aaada6a81c351946028e34f9c18610fce2c4573))

* &#39;Refactored by Sourcery&#39; ([`58961df`](https://github.com/mraniki/talkytrend/commit/58961dff715fa6e2e4229860bd42db6db1822bfd))

* Merge pull request #65 from mraniki/dev

📝 readmydocs updated config ([`98f1cb1`](https://github.com/mraniki/talkytrend/commit/98f1cb156c43fbad64358b913e6709acba3c82d1))

* Updating Requirements ([`9f18870`](https://github.com/mraniki/talkytrend/commit/9f1887053f4f95c4f53a5751e4c4cee0d9754dbc))

* Merge pull request #64 from mraniki/dev

📝 readmydocs updated config ([`9a3d209`](https://github.com/mraniki/talkytrend/commit/9a3d209a333cd50063ace30b31a3e5817cf37a1b))

### Update

* Update README.md ([`b2bbbdb`](https://github.com/mraniki/talkytrend/commit/b2bbbdbce68a9846db6324b25a372a6c6736051b))

* :memo:  Update Requirements ([`006c5fb`](https://github.com/mraniki/talkytrend/commit/006c5fb31434191039dedb82d6b14ff16ccafea0))

### ♻️

* ✅ ♻️  unit test loop break ([`1303c70`](https://github.com/mraniki/talkytrend/commit/1303c70d60ca9c34eeee571b50aac2b1d51ba3dd))

* ♻️ Reinforce the scanner stop capability ([`b4aed6d`](https://github.com/mraniki/talkytrend/commit/b4aed6d180e7c201f06482f22ebfdf951fdfb866))

* ♻️ self scanning cleanup ([`856368f`](https://github.com/mraniki/talkytrend/commit/856368fd41b52531d5c2e54247c70ef8cd5fdc99))

* ♻️ self.scanning attribute ([`2db5b0d`](https://github.com/mraniki/talkytrend/commit/2db5b0d463b74339d3dfbb6d8f6884fdd0bc1218))

* ♻️  xmltodict ([`11d0c2c`](https://github.com/mraniki/talkytrend/commit/11d0c2c34085750d97de58e5100b1ffe65a9776e))

### ✅

* ✅ scanner test ([`0cc962b`](https://github.com/mraniki/talkytrend/commit/0cc962bdd65659a9a53399c95fe78e4e2d657191))

* ✅  scanner update ([`a9ca19e`](https://github.com/mraniki/talkytrend/commit/a9ca19e7c972c40569b4a9621f3486005ab0c573))

* ✅  linter ([`d43b8f0`](https://github.com/mraniki/talkytrend/commit/d43b8f0856562322e072f6c76cf0cf4f7a7c82e3))

* ✅ unit test and example ([`d56d0bc`](https://github.com/mraniki/talkytrend/commit/d56d0bc83b903f452524cfe9dbb3ed12303886c0))

* ✅  increase test unit ([`2b6a24a`](https://github.com/mraniki/talkytrend/commit/2b6a24aa047d76b153ddbc5d098f995449b3af32))

* ✅ ([`440f0b7`](https://github.com/mraniki/talkytrend/commit/440f0b7577088ef923acd313423b565e12db8351))

* ✅  improve the test scanner ([`4d7442c`](https://github.com/mraniki/talkytrend/commit/4d7442cc50c255466363fc3139b21885e6439226))

* ✅  test update ([`eb54f74`](https://github.com/mraniki/talkytrend/commit/eb54f74fa9bc2a349d67f9a6f62899b855abe908))

* ✅  for news ([`7387e31`](https://github.com/mraniki/talkytrend/commit/7387e31897b1427fa70392a3eb15026311842a11))

* ✅ ([`450f630`](https://github.com/mraniki/talkytrend/commit/450f630c8bc77d3abde2790600385c1f423034b3))

* ✅ scanner unit test ([`c50b973`](https://github.com/mraniki/talkytrend/commit/c50b9730c0a244fd4a2db713ddf0210814206fbe))

* ✅ added test timeout ([`829a8cc`](https://github.com/mraniki/talkytrend/commit/829a8cc810a5dbbb028957b8cf68d20cc16ad872))

* ✅  unit test case update ([`9abc0a9`](https://github.com/mraniki/talkytrend/commit/9abc0a912e631454882f5d8096a76d20e2748c05))

* ✅  added unit test ([`5d85c42`](https://github.com/mraniki/talkytrend/commit/5d85c426a8775809a6f5f5a57c6ae767836e880c))

### 🐛

* 🐛 xmltodict version issue ([`26403a7`](https://github.com/mraniki/talkytrend/commit/26403a705cf20cab41d96865605d82272c95fcb2))

### 💄

* ✨ 💄 Delivery of the trading view trend as html table + feeds news ([`36ba590`](https://github.com/mraniki/talkytrend/commit/36ba590d2f73db16cb26fede0cdeb94848fc9edd))

### 📝

* 📝 readmydocs updated config ([`a676ea4`](https://github.com/mraniki/talkytrend/commit/a676ea44de87bbd2b79d83970649ae9c29369157))

### 🔥

* 🔥 removing test ([`7cf199a`](https://github.com/mraniki/talkytrend/commit/7cf199a40a296981591807e4a74dc17ae0eadaa2))

### 🔧

* 🔧 testing ([`d0279dc`](https://github.com/mraniki/talkytrend/commit/d0279dc9ba891f92428b61b197269fbf3ff0d3da))

* 🔧  setting update ([`ad37f98`](https://github.com/mraniki/talkytrend/commit/ad37f98500952733992afde65db9036775403666))

* 🔧 settings updated for frequency ([`401567a`](https://github.com/mraniki/talkytrend/commit/401567a10449287cba613ac913ff8f6bcf0eae52))

### 🚨

* 🚨 linter ([`d59f931`](https://github.com/mraniki/talkytrend/commit/d59f93130b7f1c4badd63f0d438ace57137c892f))

* 🚨  linter cleanup ([`089706f`](https://github.com/mraniki/talkytrend/commit/089706f3675b1f741e578cc0dda5c1c2259bc848))


## v1.6.0 (2023-07-07)

### :rotating_light:

* :rotating_light: linter ([`a78ac49`](https://github.com/mraniki/talkytrend/commit/a78ac49ef2f4e49c8d7731174d492f41dcdab046))

### Other

* Merge pull request #63 from mraniki/dev ([`d8bf47f`](https://github.com/mraniki/talkytrend/commit/d8bf47f9a5e43c31a380ca9a5a8fe54daa8b76b5))

### 🐛

* 🐛 Troubleshooting for news. ([`b13388e`](https://github.com/mraniki/talkytrend/commit/b13388e970ec61d4252c7421c335dd16e8172970))

### 🚨

* 🚨 linter ([`91c34c1`](https://github.com/mraniki/talkytrend/commit/91c34c1d65cb939a1a19aa6976b8fb592a1f2402))

### 🥚

* 🥚 Refactor TalkyTrend class to return appropriate arrows based on analysis summary recommendation. ([`9c68bd4`](https://github.com/mraniki/talkytrend/commit/9c68bd4c49492de78032ff0a5e133901fd86cfe2))


## v1.5.9 (2023-07-06)

### Other

* Merge pull request #62 from mraniki/dev

📝  Readme Alignment with Wiki structure ([`af472db`](https://github.com/mraniki/talkytrend/commit/af472db335afec4e6a643077f7483d030e8511ac))

### Update

* Update README.md ([`f2af83d`](https://github.com/mraniki/talkytrend/commit/f2af83d8cf0fb70297fd7f3a449f1b3470c9e0c4))

### 📝

* 📝  Readme Alignment with Wiki structure ([`0bf02a4`](https://github.com/mraniki/talkytrend/commit/0bf02a4309d1b00a22d16cac72531ec84bea77b2))


## v1.5.8 (2023-07-03)

### :memo:

* :memo: Add trading view connectivity with signal scanner, news connectivity, and FOMC reminder ([`d774b02`](https://github.com/mraniki/talkytrend/commit/d774b02272aba452f19b9b664829e5503a99bea3))

### Other

* Merge pull request #61 from mraniki/dev

⚡ added getinfo and docs ([`1abcd6a`](https://github.com/mraniki/talkytrend/commit/1abcd6ad56ac535d75a6eabb5849fb5a4332e1aa))

### ⚡

* ⚡ added getinfo ([`bf3a71a`](https://github.com/mraniki/talkytrend/commit/bf3a71a78c3ef8e00982c12a577412c44b0f7124))

### 📝

* 📝  Doc config ([`ee9e7a2`](https://github.com/mraniki/talkytrend/commit/ee9e7a236359f92564b618b18a36e5aecf217888))


## v1.5.7 (2023-07-03)

### 👷

* 👷 CI Change ([`8a2af72`](https://github.com/mraniki/talkytrend/commit/8a2af721abebb9de83ffba3c1d88b778a980e492))


## v1.5.6 (2023-07-03)

### 🔧

* 🔧  config  update ([`d963960`](https://github.com/mraniki/talkytrend/commit/d963960b83390d6c797ae2723aa9f1f9485ebf9f))


## v1.5.5 (2023-07-02)

### Other

* Merge pull request #60 from mraniki/dev

🐛 html news link missing &#39; ([`c4952a8`](https://github.com/mraniki/talkytrend/commit/c4952a8fe9a3d07a2dadb2bfd828ad3045b964fc))

### 🐛

* 🐛 html news link missing &#39; ([`1968474`](https://github.com/mraniki/talkytrend/commit/1968474f7f8ccd3bf138382ddbd49969721f6bbb))


## v1.5.4 (2023-07-02)

### Fix

* :art: Refactor TalkyTrend class and fix link formatting ([`4b1c2f6`](https://github.com/mraniki/talkytrend/commit/4b1c2f6924318b06eb9b2ca32c5d79741385550d))

### Other

* Merge pull request #59 from mraniki/dev

:art: Refactor TalkyTrend class and fix link formatting ([`0754e8c`](https://github.com/mraniki/talkytrend/commit/0754e8cdbd501e6581bc0125523193f14aeab226))


## v1.5.3 (2023-06-27)

### Other

* Merge pull request #58 from mraniki/dev

:art: Update default_settings.toml and main.py files ([`3832cd8`](https://github.com/mraniki/talkytrend/commit/3832cd802d8b722cce413aa5401894bedf8a86e7))

### Update

* :art: Update default_settings.toml and main.py files ([`d3836cd`](https://github.com/mraniki/talkytrend/commit/d3836cd08d864b9feb3a70787c93b73581cb2d66))

### 🎨

* 🎨 code format ([`dc0ef52`](https://github.com/mraniki/talkytrend/commit/dc0ef520ed428481929420b379945a379bb1f3c4))


## v1.5.2 (2023-06-26)

### Other

* Merge pull request #57 from mraniki/dev

Update README.md ([`7914960`](https://github.com/mraniki/talkytrend/commit/791496013d3472cb3bff1503ebd4f55f69a41ef7))

* Merge pull request #56 from mraniki/dev

👷‍♂️‍♂️Flow.yml ([`085da39`](https://github.com/mraniki/talkytrend/commit/085da3931426b09ef713c27d26d65f29658a30ab))

* :construction_worker_man:‍♂️Flow.yml ([`33c66c4`](https://github.com/mraniki/talkytrend/commit/33c66c43770b83649d98d8e29b60b1cab879adee))

### Update

* Update dependabot.yml ([`638241b`](https://github.com/mraniki/talkytrend/commit/638241b3904f6cf43ac7533a59867ac3e2411162))

* Update README.md ([`40a3163`](https://github.com/mraniki/talkytrend/commit/40a31630a31da7cf5a869b242c4a8998c60dd222))

* Update README.md ([`ea84818`](https://github.com/mraniki/talkytrend/commit/ea8481889084fd401c5d083a2c90d3ecad009596))


## v1.5.1 (2023-06-25)

### Fix

* Update dependabot.yml with commit message prefix ([`1ef1f0b`](https://github.com/mraniki/talkytrend/commit/1ef1f0b0c8f71e4b81b02519f2e7feb297ed9fdf))

### Other

* Merge pull request #55 from mraniki/dev

Update dependabot.yml with commit message prefix ([`d45fc3a`](https://github.com/mraniki/talkytrend/commit/d45fc3abfcb3aad22ba66dc267d7c26c3507c6dc))


## v1.5.0 (2023-06-25)

### ✨

* Update ✨Flow.yml ([`b631151`](https://github.com/mraniki/talkytrend/commit/b631151bc1867954dbd268c0730dec6d7ea3ab95))


## v1.4.0 (2023-06-23)

### Other

* Merge pull request #52 from mraniki/dev

🥚fomc day ([`4641813`](https://github.com/mraniki/talkytrend/commit/4641813d89259c6ee634112e146699a2031a52eb))

* Merge pull request #54 from mraniki/sourcery/dev ([`672789d`](https://github.com/mraniki/talkytrend/commit/672789db9f281a23b4266e3baf8c090ae357bcec))

* &#39;Refactored by Sourcery&#39; ([`14bd2f8`](https://github.com/mraniki/talkytrend/commit/14bd2f8d8f6785b02a4d71cba2008e07b79303a3))

* Merge pull request #53 from mraniki/sourcery/dev

🥚fomc day (Sourcery refactored) ([`1bfd73f`](https://github.com/mraniki/talkytrend/commit/1bfd73f9523c1d88bb686246ed74009c370a464b))

* &#39;Refactored by Sourcery&#39; ([`643ed12`](https://github.com/mraniki/talkytrend/commit/643ed1295338c6e17a44a34a4f392ce54973ab98))

### ✅

* ✅ unit test ([`a0c8034`](https://github.com/mraniki/talkytrend/commit/a0c80347c86c8e51945acd5fd2952b21755b1c3a))

### 🥚

* 🥚fomc day ([`bc97764`](https://github.com/mraniki/talkytrend/commit/bc977641152814d5a8060a72b8079c8dbd92810c))


## v1.3.5 (2023-06-22)

### :memo:

* :memo: docs ([`d93df5b`](https://github.com/mraniki/talkytrend/commit/d93df5b37b9e069bf35da9ae129b3ad228f5be34))

* :memo: readthedocs ([`3b9998e`](https://github.com/mraniki/talkytrend/commit/3b9998e2c381b585d760d1a606b30ba377e788b8))

### Other

* Merge pull request #51 from mraniki/dev

📝 readthedocs ([`a2191fa`](https://github.com/mraniki/talkytrend/commit/a2191fabdba69b545ce2c8a1380b8a799a933898))

* Merge branch &#39;dev&#39; of git@github.com:mraniki/talkytrend.git ([`d935804`](https://github.com/mraniki/talkytrend/commit/d935804921f14b70b4229f1ff62449a305cc07dd))


## v1.3.4 (2023-06-22)

### Other

* Merge pull request #50 from mraniki/dev

📝 documentation ([`0f5cc2e`](https://github.com/mraniki/talkytrend/commit/0f5cc2ec81f66b4a2a32def745f413943482340b))

### 💬

* 💬 doc ([`9890909`](https://github.com/mraniki/talkytrend/commit/9890909b89f740d9a6abdceb66bd1d7420dce81b))

* 💬 doc ([`49b8127`](https://github.com/mraniki/talkytrend/commit/49b8127e1ac2850d843a423c9706a408e84b004c))


## v1.3.3 (2023-06-21)

### :arrow_up:

* :arrow_up: dep update ([`8ac66b0`](https://github.com/mraniki/talkytrend/commit/8ac66b07861be4c3c3d117977dbf8fd1bf2f4ddf))

* :arrow_up:dep update ([`71d69e1`](https://github.com/mraniki/talkytrend/commit/71d69e159b8c2b2ecf2278d62f35f59749454807))

### :memo:

* :memo: docs ([`5f7d4ef`](https://github.com/mraniki/talkytrend/commit/5f7d4ef691faef18c4662f12417211ba8eb1002b))

### Other

* Merge pull request #46 from mraniki/dev

📝 docs ([`f73885e`](https://github.com/mraniki/talkytrend/commit/f73885eae52ce647b35ed6f8b0d367079ae1f90b))

* Merge branch &#39;main&#39; into dev ([`662bde9`](https://github.com/mraniki/talkytrend/commit/662bde951c021983149d422f42e321f7f2545332))

* Create test.txt ([`0a37dca`](https://github.com/mraniki/talkytrend/commit/0a37dcac46d913b1119409ec21cce9ff7d58efef))

* Delete html ([`f6d8b54`](https://github.com/mraniki/talkytrend/commit/f6d8b548e77be7fe67aa2057f949b98e823ed5f8))

* Merge pull request #45 from mraniki/dev ([`3be012c`](https://github.com/mraniki/talkytrend/commit/3be012c62e501ac175f92c0d6f413da27b39fd7f))

* Merge pull request #44 from mraniki/dev

 ⬆️ Update pyproject.toml ([`78485c1`](https://github.com/mraniki/talkytrend/commit/78485c1b7173c8577438273c06e7e44f98251c9a))

### Update

* Update Docs html ([`8709c1b`](https://github.com/mraniki/talkytrend/commit/8709c1b18b8c31ba0d674ad81839e5bb53f2ff6b))

* Update pyproject.toml ([`3df4d59`](https://github.com/mraniki/talkytrend/commit/3df4d59d48618afbdb80500fd8bba0da12729eb4))


## v1.3.2 (2023-06-17)

### Other

* Merge pull request #41 from mraniki/dev

🔧 config ([`3127f6a`](https://github.com/mraniki/talkytrend/commit/3127f6a78129e4701343323882ac4568f2d51d18))

### 🔧

* 🔧 config ([`9996677`](https://github.com/mraniki/talkytrend/commit/9996677dd584173e00353430e6c200f06626d971))


## v1.3.1 (2023-06-15)

### Other

* Merge pull request #39 from mraniki/dev

🔧 config org ([`e18bd19`](https://github.com/mraniki/talkytrend/commit/e18bd1902dda5893c2cce082e9ba83d3602204b7))

### 🎨

* 🎨 code format ([`3678446`](https://github.com/mraniki/talkytrend/commit/36784460d05220ef82aadfc7e91ca6337cde4604))

### 🔧

* 🔧 config org and removed logs ([`6ac7f8f`](https://github.com/mraniki/talkytrend/commit/6ac7f8f60ad0e6eae94c7a4a99350e6f8be67cc4))


## v1.3.0 (2023-06-15)

### Other

* Merge pull request #38 from mraniki/dev

:wrench: Update scanner frequency and enable news in default settings. ([`1d7ec8d`](https://github.com/mraniki/talkytrend/commit/1d7ec8dbd4027c855d041a67b427d5aabc6ba1fa))

* Merge pull request #36 from mraniki/dev

🥚 Refactor TalkyTrend class and add Markdown support to table output ([`63aec14`](https://github.com/mraniki/talkytrend/commit/63aec144d614c1996fd0d869145f988b24838d5c))

### Update

* :wrench: Update scanner frequency and enable news in default settings. ([`0e48f29`](https://github.com/mraniki/talkytrend/commit/0e48f29c79512554785d7f41ed2f75e0d9cc72bf))

### 🥚

* 🥚 Refactor TalkyTrend class and add Markdown support to table output ([`9a4105a`](https://github.com/mraniki/talkytrend/commit/9a4105ad13dea76e19fbff1e8fa72ef22cca568a))


## v1.2.1 (2023-06-14)

### Other

* Merge pull request #35 from mraniki/dev

🔧 config for enabling disabling the lib ([`ca57682`](https://github.com/mraniki/talkytrend/commit/ca57682dae13e22369eb07cfa990da42c03cf86e))

### 🔧

* 🔧 config for enabling disabling the lib ([`b8ccc8a`](https://github.com/mraniki/talkytrend/commit/b8ccc8ab5328181051ea5fadce452c1976104a8e))


## v1.2.0 (2023-06-14)

### Other

* Merge pull request #34 from mraniki/dev ([`0c2037b`](https://github.com/mraniki/talkytrend/commit/0c2037b4b8b6285f953dcbd4ef3b29bf83885d1f))

### Update

* Update Dockerfile ([`901d4a1`](https://github.com/mraniki/talkytrend/commit/901d4a1035bf5fcdc7bf1ba685a1a2edebd78de0))

* Update Dockerfile ([`29c566b`](https://github.com/mraniki/talkytrend/commit/29c566b4e3249e8c1dc21b65ba9679873d60bced))

* Update Dockerfile ([`884bcae`](https://github.com/mraniki/talkytrend/commit/884bcae9c1f4cde68991d60deac31e5df1ff54f2))

* Update Dockerfile ([`e788463`](https://github.com/mraniki/talkytrend/commit/e788463301b06d93fbbb4aece663c8fe3eb3efbc))

* Update Dockerfile ([`5660a50`](https://github.com/mraniki/talkytrend/commit/5660a5048b4255b9938e3ced250c03d689cafe66))

### ⚡

* ⚡perf ([`43568fe`](https://github.com/mraniki/talkytrend/commit/43568fe5690f131da42572975d68de65f45c5d57))

### ✨

* ✨ adding pretty table support ([`7552491`](https://github.com/mraniki/talkytrend/commit/75524918f43b0ab1608c5a599fbaa77236cfb5d0))

### 🎨

* 🎨 code format ([`6be8713`](https://github.com/mraniki/talkytrend/commit/6be87133969a5411cddc56154e3bd735f8a35c2f))

* 🎨 code format ([`17d547d`](https://github.com/mraniki/talkytrend/commit/17d547d83f55b8887cfc0dd8d74df3df593314fc))


## v1.1.9 (2023-06-14)

### :recycle:

* :recycle:🥅 Refactor error handling and logging in TalkyTrend class ([`b56e3be`](https://github.com/mraniki/talkytrend/commit/b56e3be121463e9af276bcfc3d235df1d98b61c7))

### Fix

* 🚑 fix signal ([`148a873`](https://github.com/mraniki/talkytrend/commit/148a87352205683fa790a9c4287860ba1b28f76e))

* 🐛 fix async bug and signal bug ([`27dae32`](https://github.com/mraniki/talkytrend/commit/27dae328e0c03129592a937c4c3d012aa9c5634c))

### Other

* Merge pull request #33 from mraniki/dev

🚑 fix signal ([`e6da7a1`](https://github.com/mraniki/talkytrend/commit/e6da7a16dfa0af7be3e660401edcece529670dd0))

* Rename requirements.txt to requirements.txt ([`97f1a3e`](https://github.com/mraniki/talkytrend/commit/97f1a3e7d6bc51c8e5807cbf5f70bfd817f1940f))

* Merge pull request #32 from mraniki/dev

:recycle:🥅 Refactor error handling and logging in TalkyTrend class ([`5305507`](https://github.com/mraniki/talkytrend/commit/5305507e67a4dcacb3b2cc3ab5d68d7332f3ee56))

### 🐳

* 🐳 dockerignore ([`a6dfa56`](https://github.com/mraniki/talkytrend/commit/a6dfa568aa4ea56b81ab7ed36187a4ed61a72e4a))


## v1.1.8 (2023-06-14)

### Fix

* 🚑 hot fix  Use &#39;yield&#39; to return the result as an asynchronous iterator for scanner ([`2374c72`](https://github.com/mraniki/talkytrend/commit/2374c72e4bed0dfcb6df6ac48a01d8b386297f8e))

### Other

* Merge pull request #31 from mraniki/dev

🚑 hot fix  Use &#39;yield&#39; to return async for scanner ([`8c585c9`](https://github.com/mraniki/talkytrend/commit/8c585c9943aaa3023cbf867842f27bb3105e65a0))


## v1.1.7 (2023-06-13)

### :recycle:

* :recycle: none article case ([`1bea4f8`](https://github.com/mraniki/talkytrend/commit/1bea4f87c49166c50fcb3e76f90ef9e77776975c))

### :white_check_mark:

* :white_check_mark: unit test ([`5a3795f`](https://github.com/mraniki/talkytrend/commit/5a3795fd63ccee4c33467269a219841257ff1065))

### Other

* Merge pull request #29 from mraniki/dev

♻️ none article case ([`2e543bb`](https://github.com/mraniki/talkytrend/commit/2e543bb303ee62e80869ee7743d297a0fbc3916a))

* Merge pull request #30 from mraniki/sourcery/dev

♻️ none article case (Sourcery refactored) ([`f1dfee6`](https://github.com/mraniki/talkytrend/commit/f1dfee63272642533d8392723ca56612a8eba4d0))

* &#39;Refactored by Sourcery&#39; ([`a14d0fa`](https://github.com/mraniki/talkytrend/commit/a14d0fa61c534db491d1c9faf44c96d3b7d97ae7))

* Merge branch &#39;dev&#39; of git@github.com:mraniki/talkytrend.git ([`50cafd3`](https://github.com/mraniki/talkytrend/commit/50cafd3fc3e4a8367e4283f36904cd372749003f))

### ⚗️

* ⚗️ emoji for market event ([`64ec558`](https://github.com/mraniki/talkytrend/commit/64ec5589d401ff2624b2f57a30d4585cdc0f17cf))


## v1.1.6 (2023-06-12)

### :arrow_up:

* :arrow_up: Add aiohttp to dependencies ([`8f44fc1`](https://github.com/mraniki/talkytrend/commit/8f44fc16c89c72726699814b02a9324e7ec53264))

### Fix

* ♻️ refactor and fix loop defect ([`3616948`](https://github.com/mraniki/talkytrend/commit/36169489754b3242b0b7f3c0e7107debe732b66f))

### Other

* Merge pull request #28 from mraniki/dev ([`022cf37`](https://github.com/mraniki/talkytrend/commit/022cf377358a4e671359cf77d336c1dd4c88158a))

### ✅

* ✅ unit test ([`d4bd8da`](https://github.com/mraniki/talkytrend/commit/d4bd8da4903147a2bb48d129e65cb7a29ab3ecfe))

### 💬

* 💬 doc ([`adbd2fd`](https://github.com/mraniki/talkytrend/commit/adbd2fdf28cc0bd41abd83e2d7c80776c43e921e))


## v1.1.5 (2023-06-12)

### Other

* Merge pull request #27 from mraniki/dev

🐛 return instead of yield ([`796e0cd`](https://github.com/mraniki/talkytrend/commit/796e0cdba668d67d386d6a1106f44ae8c55e826a))

### 🐛

* 🐛 return instead of yield ([`029145a`](https://github.com/mraniki/talkytrend/commit/029145a709d35856ffe49b25cee2487516eb4d45))


## v1.1.4 (2023-06-11)

### Other

* Merge pull request #26 from mraniki/dev ([`91903ce`](https://github.com/mraniki/talkytrend/commit/91903ce2b607c96239d870cf06c938f617c102f2))

### 🔊

* 🔊 add logs ([`fd8dc7c`](https://github.com/mraniki/talkytrend/commit/fd8dc7c904c82d3679a220757565a2c5515b7635))

### 🔧

* 🔧 config ([`743b2c3`](https://github.com/mraniki/talkytrend/commit/743b2c3641c375598b702f0bba527e312f0b5fdb))


## v1.1.3 (2023-06-11)

### Other

* Merge pull request #25 from mraniki/dev

♻️ refactor ([`e73ea38`](https://github.com/mraniki/talkytrend/commit/e73ea389952a88f4feab99df21dc2bbfa86555ec))

### ♻️

* ♻️ refactor ([`43b9052`](https://github.com/mraniki/talkytrend/commit/43b905205ef5b3fbd7b841e9f500d6f592a9df2d))


## v1.1.2 (2023-06-11)

### Other

* Merge pull request #24 from mraniki/dev

🔧 config ([`15d7129`](https://github.com/mraniki/talkytrend/commit/15d71293dc13914fcd71264be398937ef622e7ca))

* Merge pull request #23 from mraniki/dev

Update default_settings.toml ([`5b364b2`](https://github.com/mraniki/talkytrend/commit/5b364b28ced3f2a5fc39b0f0ee45ffccc7bd2d88))

### Update

* Update default_settings.toml ([`a2910b5`](https://github.com/mraniki/talkytrend/commit/a2910b5b912825aa94d23c13d41aa4de3900ff63))

### ⚗️

* ⚗️ live tv link ([`37c49fa`](https://github.com/mraniki/talkytrend/commit/37c49faba19c1380ad470aabf223f537207e9319))

### 🔧

* 🔧 config ([`cb5a0b1`](https://github.com/mraniki/talkytrend/commit/cb5a0b10b2f5dbd02bf2fc39054f5efab7d04204))


## v1.1.1 (2023-06-10)

### Other

* Merge pull request #22 from mraniki/dev

♻️ refactor ([`05fdf0a`](https://github.com/mraniki/talkytrend/commit/05fdf0a2e8f20940d349c38847d9a7965bc324c4))

### ♻️

* ♻️ refactor ([`9940b58`](https://github.com/mraniki/talkytrend/commit/9940b58928b2b66c3daac1c3d84be1c5a6f7c369))

* ♻️ refactor ([`0f70d7c`](https://github.com/mraniki/talkytrend/commit/0f70d7c2332eb9c24e2edd6a708934fbec71fcc7))

### ✅

* ✅ unit test ([`756e629`](https://github.com/mraniki/talkytrend/commit/756e629ca9c97e2206ce745cb5a20ee4199f9c8c))

### 💬

* 💬 doc ([`6529043`](https://github.com/mraniki/talkytrend/commit/65290435310566ce8d9bdf15d26ace9749494075))

* 💬 doc ([`1553373`](https://github.com/mraniki/talkytrend/commit/1553373a7e546193e303743313e2cb64aa9e408a))


## v1.1.0 (2023-06-10)

### :recycle:

* :recycle:  Remove unnecessary comment in TalkyTrend class. ([`806aabf`](https://github.com/mraniki/talkytrend/commit/806aabfc68b772f7a05daa195051ae12549e29e6))

### Feat

* 🥚prepping new feat breaking calendar and news ([`7acb397`](https://github.com/mraniki/talkytrend/commit/7acb3970bac56c820650f91cd21cacbae2f1536a))

### Fix

* 🚑 hot fix ([`e72aef8`](https://github.com/mraniki/talkytrend/commit/e72aef809a3109c28033c943b30a7e20c2bbe2b5))

### Other

* Merge pull request #21 from mraniki/dev

🥚prepping new feat breaking calendar and news ([`4047630`](https://github.com/mraniki/talkytrend/commit/40476300294f9e2cdec0b6af9b33601a821b22e1))

* Merge pull request #20 from mraniki/dev

Update README.md ([`1ae1860`](https://github.com/mraniki/talkytrend/commit/1ae186011fc8bd1099b22207a689f21dd800e17d))

### Update

* Update pyproject.toml ([`35aaa4f`](https://github.com/mraniki/talkytrend/commit/35aaa4f5c0081dded56426bebde4a6cc59bd8f6d))

* Update pyproject.toml ([`4dc8739`](https://github.com/mraniki/talkytrend/commit/4dc8739ad1acf26d280dfad7198838c0956a812f))

* Update pyproject.toml ([`d123dcc`](https://github.com/mraniki/talkytrend/commit/d123dcc983f731c1519233051a85e3ea04bc692c))

* :speech_balloon: Update pyproject.toml with improved package description and removed unused dependency ([`323faba`](https://github.com/mraniki/talkytrend/commit/323faba55f63ad2a675f987be26e70615006fd29))

* Update README.md ([`a94c461`](https://github.com/mraniki/talkytrend/commit/a94c4614932ea5028ec825432a81a64bff06e72f))

### ✅

* ✅ unit test ([`ffae900`](https://github.com/mraniki/talkytrend/commit/ffae90027a91a9614ca2b419d78e4802362734ca))

### 🎨

* 🎨 code format ([`15866e5`](https://github.com/mraniki/talkytrend/commit/15866e5540207bf03d5622f7e73828b7476963e2))

### 🐛

* 🐛 bug ([`d487a17`](https://github.com/mraniki/talkytrend/commit/d487a173d101ef427785c7f4150f32f94c851d7b))


## v1.0.0 (2023-06-08)

### Other

* Merge pull request #19 from mraniki/dev

💥 initial tv monitoring ([`971ba77`](https://github.com/mraniki/talkytrend/commit/971ba77873175b1350356767da7098661dd10ea8))

### ♻️

* ♻️ refactor ([`d6aec00`](https://github.com/mraniki/talkytrend/commit/d6aec00495175924fe58111ffb624e8d98934f4d))

### 💥

* 💥 initial tv monitoring ([`97955e3`](https://github.com/mraniki/talkytrend/commit/97955e3053b9d7793a3f270bfadcdf2d23f0c252))

### 🔧

* 🔧 config ([`4d65c62`](https://github.com/mraniki/talkytrend/commit/4d65c62477f9f7ebfe07f5f9054264dfe6b455b0))


## v0.0.3 (2023-06-08)

### Other

* Merge pull request #18 from mraniki/dev

♻️ refactor ([`823de2b`](https://github.com/mraniki/talkytrend/commit/823de2b69c1f79e4226031edad42f72c010d86f3))

### ♻️

* ♻️ refactor ([`02ed9c8`](https://github.com/mraniki/talkytrend/commit/02ed9c8ffe1dc1b09f3284106481ddcfae986822))

* ♻️ refactor ([`25889d9`](https://github.com/mraniki/talkytrend/commit/25889d98622c83b1c111a01b7230536c7bc2ba6e))


## v0.0.2 (2023-06-08)

### Other

* Merge pull request #17 from mraniki/dev

✅ unit test ([`01f8c06`](https://github.com/mraniki/talkytrend/commit/01f8c0618ec5e3f042d9d2bd8f8d1e9861711860))

### Update

* Update README.md ([`1cdbe1d`](https://github.com/mraniki/talkytrend/commit/1cdbe1d746c14d0889eb03c4ce9657135a15ac33))

### ♻️

* ♻️ refactor ([`3333b3e`](https://github.com/mraniki/talkytrend/commit/3333b3e38af9fade1a821f1f020f95357931457b))

### ✅

* ✅ unit test ([`1a2ce47`](https://github.com/mraniki/talkytrend/commit/1a2ce47b3c4ffcf6cf91ed095d48cd01f7551859))

* ✅ unit test ([`24afabc`](https://github.com/mraniki/talkytrend/commit/24afabc6db39bca08b41466bdfb1c22a2707d478))


## v0.0.1 (2023-06-08)

### :arrow_up:

* :arrow_up: Fix exchange parameter in TrendPlugin constructor ([`46d89e8`](https://github.com/mraniki/talkytrend/commit/46d89e865662d1c2927e01ee8c8cea6f81624f4f))

* :arrow_up: Fix typo in test_fetch_analysis_crypto() function. ([`903f65f`](https://github.com/mraniki/talkytrend/commit/903f65f8b3bdadfb4d48c44239ca0dbaca77b0aa))

### :boom:

* :boom: Remove broken badges from README.md file ([`e12435e`](https://github.com/mraniki/talkytrend/commit/e12435e443613fb9eab5f0849e7256d5d238d7c8))

* :boom: Update issue template and remove Dependabot configuration ([`8401887`](https://github.com/mraniki/talkytrend/commit/84018876ff1d3f310a144ac681206923a56bce63))

### :bug:

* :bug: Refactor test_fetch_analysis function ([`576d208`](https://github.com/mraniki/talkytrend/commit/576d2080a3a3d2366df35cb42dfe9bd2c6516ab6))

* :bug: Refactor TrendPlugin to use FX_IDC exchange and add interval parameter. ([`239b9eb`](https://github.com/mraniki/talkytrend/commit/239b9ebcf012f9a5b0b2cd218311d09e64cbbf8b))

* :bug: Refactor test_unit.py imports and patching ([`da58b12`](https://github.com/mraniki/talkytrend/commit/da58b127f3d1fa5575a642bdf181560f75f8ee79))

### :fire:

* :fire: Remove httpx import in main.py ([`a852f96`](https://github.com/mraniki/talkytrend/commit/a852f9641420396664407dc0f3582dc40c34c1f9))

### :memo:

* :memo: Add package description and badges ([`0fbcd91`](https://github.com/mraniki/talkytrend/commit/0fbcd91c5b7260fed3c12496b75e9585129ba989))

### :recycle:

* :recycle: Refactor TrendPlugin class and default_settings.toml ([`ea45f3b`](https://github.com/mraniki/talkytrend/commit/ea45f3b4ab3d1277e0b02de8697bd93f78b3fb38))

* :recycle: Refactor TrendPlugin instantiation and update default assets ([`6faf007`](https://github.com/mraniki/talkytrend/commit/6faf007f4651e525822386b51c91e6bf797c1039))

### :white_check_mark:

* :white_check_mark: Refactor test_fetch_analysis function in test_unit.py ([`e6cdc5e`](https://github.com/mraniki/talkytrend/commit/e6cdc5e7a1d82bfdc2347d3026b5fd963a474232))

### :wrench:

* :wrench::white_check_mark: Refactor TrendPlugin init method to accept default assets ([`7177d4d`](https://github.com/mraniki/talkytrend/commit/7177d4dad812988fe16e16cb5071ae89397a3042))

### :zap:

* :zap: Refactor example.py and requirements.txt for talkytrend plugin. ([`1874702`](https://github.com/mraniki/talkytrend/commit/187470248836b5376ed2cf864ac9fb9c905f615a))

### Fix

* symboltrend fix ([`fa3d9f8`](https://github.com/mraniki/talkytrend/commit/fa3d9f8fb76b41f9ceb1c71a4659d7f4e1a45c2d))

### Other

* Merge pull request #15 from mraniki/dev

👷 CI Change ([`fdd476d`](https://github.com/mraniki/talkytrend/commit/fdd476d18f0d09da9dd1e058a88ee767edcf3ad9))

* Merge pull request #16 from mraniki/main

👷 CI Change ([`3726d91`](https://github.com/mraniki/talkytrend/commit/3726d912cf858fe5639f27c38db65eb48fa5586a))

* Merge pull request #14 from mraniki/dev

:boom: Remove broken badges from README.md file ([`4755af3`](https://github.com/mraniki/talkytrend/commit/4755af31f2b72e45df15ffc507fcf1000fc59d13))

* Merge pull request #13 from mraniki/dev

Dev ([`eb65612`](https://github.com/mraniki/talkytrend/commit/eb6561265c5ebc21677ec867e8b5ffc845f00dd8))

* Merge branch &#39;dev&#39; of git@github.com:mraniki/TalkyTrend.git ([`943d0b4`](https://github.com/mraniki/talkytrend/commit/943d0b4b77d9a67fad4c0fff20106e2df1747934))

* :construction_worker_man:‍♂️ CI ([`3ce7867`](https://github.com/mraniki/talkytrend/commit/3ce7867f58685199d48f73ab396698616c15fb22))

* Add forex screener to TrendPlugin. ([`97b0410`](https://github.com/mraniki/talkytrend/commit/97b041056ede3354fe71dfbf200b76c5341c8cd6))

* Merge pull request #12 from mraniki/dev ([`53d38e9`](https://github.com/mraniki/talkytrend/commit/53d38e98aa885a809b7ce5a87273ebd42aa7e2d7))

* Refactor default_settings.toml and test_unit.py files. ([`58fd6af`](https://github.com/mraniki/talkytrend/commit/58fd6af752605d2438b40077ab67d72e960f4fb2))

* Merge pull request #7 from mraniki/dev ([`6f8ebb9`](https://github.com/mraniki/talkytrend/commit/6f8ebb997ba8b3e308b1df84acd0fc129fc5c156))

* Merge pull request #8 from mraniki/renovate/configure

Configure Renovate ([`17f1b0f`](https://github.com/mraniki/talkytrend/commit/17f1b0f9a673daed5300651adc4c7b6b1e84f1f9))

* Add renovate.json ([`5eb13da`](https://github.com/mraniki/talkytrend/commit/5eb13dae727be02536b92cbdfe9124dd2b6dee50))

* Refactor test_unit.py for async testing ([`c88ad51`](https://github.com/mraniki/talkytrend/commit/c88ad511b281ccce1e73d2da1fb44879fa4cac01))

* Refactor bot.py to import HTMLResponse from fastapi.responses ([`0c2ff27`](https://github.com/mraniki/talkytrend/commit/0c2ff271eb1d9f34b55a167726cf4eca394ebe42))

* Add HTML response class to root endpoint ([`a0aa0b7`](https://github.com/mraniki/talkytrend/commit/a0aa0b79c1d0d3504495ae73ff8677dd93f72e6c))

* Refactor get_dashboard to return HTML table ([`cc89f0d`](https://github.com/mraniki/talkytrend/commit/cc89f0d41b179111313bccfb704580ee558c93a9))

* Fix get_dashboard function return value. ([`e35944a`](https://github.com/mraniki/talkytrend/commit/e35944a25da9981dfcb8ca74485e2b89cd047ed2))

* Refactor get_dashboard and read_root functions. ([`82c3d81`](https://github.com/mraniki/talkytrend/commit/82c3d819b19b951581d4d6147fbb05bc081715cd))

* Merge branch &#39;main&#39; of https://github.com/mraniki/TalkyTrend ([`1687d78`](https://github.com/mraniki/talkytrend/commit/1687d78c205a92c92fa2a8f891bcfaf52928821e))

* update ([`4bcce5b`](https://github.com/mraniki/talkytrend/commit/4bcce5b6ddaa6adea0dc937026227f7ce21c9148))

* Merge branch &#39;main&#39; of git@github.com:mraniki/TalkyTrend.git ([`a7a1102`](https://github.com/mraniki/talkytrend/commit/a7a11024e0361b51aa2b15a764518152ba39d1d3))

* Remove gspread_dataframe import from bot.py ([`75cff6a`](https://github.com/mraniki/talkytrend/commit/75cff6a8e066cf66d1e6249a5d10c4bdaa858ee6))

* Add auto requirement file step to DockerHub workflow ([`e5a9b48`](https://github.com/mraniki/talkytrend/commit/e5a9b48728a64dfac1d0b7ac32e076c47a182845))

* update ([`da1726a`](https://github.com/mraniki/talkytrend/commit/da1726ac81204f254c4e549b28a8631adba7b968))

* Merge branch &#39;main&#39; of https://github.com/mraniki/TalkyTrend ([`c2f8190`](https://github.com/mraniki/talkytrend/commit/c2f8190b1b9c5938b999de495bdd5a184003079e))

* update ([`380e0c1`](https://github.com/mraniki/talkytrend/commit/380e0c1e2971657e71ceb706fa406e1b15a8a5ee))

* Refactor viewer_news function to read news from JSON and save to CSV. ([`78b0426`](https://github.com/mraniki/talkytrend/commit/78b0426976f283cf11ea33c88bf74b25e74836df))

* Added pandas to requirements.txt and imported pandas in bot.py, also added a function to view news in bot.py. ([`f264ccb`](https://github.com/mraniki/talkytrend/commit/f264ccbf85acf780fbcecd2a652a8f13c6f36df8))

* update ([`90cdd31`](https://github.com/mraniki/talkytrend/commit/90cdd310fa85a1626eda5f5cdd9eb89edc06166a))

* microdot ([`006986e`](https://github.com/mraniki/talkytrend/commit/006986ea60cb885570cb2cdbfd2a2c457fdb350d))

* microdot ([`14e8774`](https://github.com/mraniki/talkytrend/commit/14e8774cfa2787a4d24361c02ebead6fbbecb57d))

* pyvibe ([`712bbaf`](https://github.com/mraniki/talkytrend/commit/712bbaffbea874df5504a89ac8839ab1789222d6))

* update ([`d9c976f`](https://github.com/mraniki/talkytrend/commit/d9c976f803a6ec28427290eb82103d0a64cc4501))

* Merge branch &#39;main&#39; of https://github.com/mraniki/TalkyTrend ([`5d3ae3a`](https://github.com/mraniki/talkytrend/commit/5d3ae3a21d50d0c1651648f60473fbd2d4943bd1))

* Create .github/dependabot.yml ([`192e2f7`](https://github.com/mraniki/talkytrend/commit/192e2f78a2d9ddd8b6900c51bd0b8f10f6325fa2))

* dockerhub update ([`1abff4b`](https://github.com/mraniki/talkytrend/commit/1abff4b2c56a0b92675c55304dfb222d6beab382))

* github registry added and readme updated ([`18a2aca`](https://github.com/mraniki/talkytrend/commit/18a2aca50bf64c978117ef804f7be1544920ecd1))

* fredapi ([`237d256`](https://github.com/mraniki/talkytrend/commit/237d25633fcaf443f8d153438662861c4367d71c))

* Added FNAPI environment variable and Finnhub API client, removed TDAPI environment variable and TD Ameritrade API client, and printed recommendation trends for BTCUSD. ([`cd2803e`](https://github.com/mraniki/talkytrend/commit/cd2803e6b5d913c13a73d42e85bcb063abeb7c94))

* Added finnhub to requirements and imported in bot.py. ([`8a056ae`](https://github.com/mraniki/talkytrend/commit/8a056ae0cc9075ad2f571b8cdb08c5b2b0d1bc63))

* pretty table added ([`b362859`](https://github.com/mraniki/talkytrend/commit/b3628597858d93dc2243b9f1cd3b9d126cad4fba))

* trend added to to the get response ([`600e9ae`](https://github.com/mraniki/talkytrend/commit/600e9aea8fd11562c02e817375b2246323024ae1))

* Merge branch &#39;main&#39; of https://github.com/mraniki/TalkyTrend ([`dd82d1c`](https://github.com/mraniki/talkytrend/commit/dd82d1cb52625f556d906703f571022a58846c40))

* initial release ([`391e44f`](https://github.com/mraniki/talkytrend/commit/391e44ff1d29b61599dfdd83e8cb45a99b898384))

* Initial commit ([`bf4fbb5`](https://github.com/mraniki/talkytrend/commit/bf4fbb587006d2da03264e874bc3022d89493957))

### Update

* :zap: Update test_unit.py ([`63d445f`](https://github.com/mraniki/talkytrend/commit/63d445f91d99c5d55baaf4c3541ed8ee21a3c959))

* Update main.py ([`41560cf`](https://github.com/mraniki/talkytrend/commit/41560cf8ef3584eaa6d223f5a3d0fc2627f503c1))

* Update README.md ([`7f7fd6a`](https://github.com/mraniki/talkytrend/commit/7f7fd6a2de16805c679c15d45562367deb6d6eb6))

* :arrow_up: Update requirements for talkytrend. ([`43e8661`](https://github.com/mraniki/talkytrend/commit/43e86611a899cfb82b37ad5d1de15419cae97f92))

* Update and rename renovate.json to .github/renovate.json ([`2d3892b`](https://github.com/mraniki/talkytrend/commit/2d3892b299e9ea3f5c8b3df1940629a504988c7f))

* Update test_unit.py ([`5a0173a`](https://github.com/mraniki/talkytrend/commit/5a0173a05c0272235caac7b8ac49f9ebf9cc5a10))

* Update test_unit.py ([`2d2815f`](https://github.com/mraniki/talkytrend/commit/2d2815fd91a04dd832f16a2ed0558c088c5ecd0d))

* Update main.py ([`52aa500`](https://github.com/mraniki/talkytrend/commit/52aa5004e8ebae7da61f13034520ada29ceddca3))

* :bug: Update default_settings.toml and main.py files with new asset and TA_Handler initialization. ([`d03d857`](https://github.com/mraniki/talkytrend/commit/d03d8579acdc5b2bcbecc6426b1f7d49b96bce02))

* Update bot.py ([`59ddb3c`](https://github.com/mraniki/talkytrend/commit/59ddb3c93d39a9d609ac864c21247487e2345b25))

* Updated  requirements file ([`f9fb279`](https://github.com/mraniki/talkytrend/commit/f9fb2792fc2586ce7f5c0d090022184d396e4cdf))

* Updated  requirements file ([`a644fda`](https://github.com/mraniki/talkytrend/commit/a644fdae8742242c6d4faf6193ac3bd2b399fe84))

* Updated  requirements file ([`c9940fc`](https://github.com/mraniki/talkytrend/commit/c9940fcb8d9a24f4470ccf7b5ba05b403fe2bb67))

* Update requirements.txt with uvicorn and gspread. ([`1d93923`](https://github.com/mraniki/talkytrend/commit/1d939232aaa55d9584ea8b5e7b6d0398dff1a29f))

* Update Dockerfile ([`4ded349`](https://github.com/mraniki/talkytrend/commit/4ded349df8364653649ffdf1a66e9e3c11e6a229))

* Update requirements.txt ([`32f97c7`](https://github.com/mraniki/talkytrend/commit/32f97c7fea9c66549e34e720c3b6973babc4c4cc))

* Update requirements.txt ([`fae5947`](https://github.com/mraniki/talkytrend/commit/fae5947e9347308a6794bbb5d1bd03e23de9aad0))

* Update bot.py ([`acf8a04`](https://github.com/mraniki/talkytrend/commit/acf8a04988dd1e9fceae3297d8b7ae481716cb51))

* Update bot.py ([`e989bfb`](https://github.com/mraniki/talkytrend/commit/e989bfb917469d5c9e2d53c006c9dc964047ec0f))

* Update bot.py ([`2835d62`](https://github.com/mraniki/talkytrend/commit/2835d62ceea52f53ac4f0dd1059557a18cba40a6))

* Update bot.py ([`33ba993`](https://github.com/mraniki/talkytrend/commit/33ba9936a064c912a63b93f8d6c8e516ce9e26d3))

* Update bot.py ([`9ff67e6`](https://github.com/mraniki/talkytrend/commit/9ff67e6756d90d6833b5b30a157d79461219c7f5))

* Update bot.py ([`db4d6a2`](https://github.com/mraniki/talkytrend/commit/db4d6a2445b99ba5f4c6a7e703bf7676d0153ea5))

* Update bot.py ([`f1e1439`](https://github.com/mraniki/talkytrend/commit/f1e1439407b5d95cec2cb5dba20ba15f7370c628))

* Update README.md ([`b139159`](https://github.com/mraniki/talkytrend/commit/b1391590c6aa91efae8830af50a36b87c938670b))

* Update Dockerfile ([`6a3177c`](https://github.com/mraniki/talkytrend/commit/6a3177c56a7ca9856b121a077429f7b59c81f371))

* Update README.md ([`8014abb`](https://github.com/mraniki/talkytrend/commit/8014abb24141ed95a2399c70a93220d91c5f6840))

* Update README.md ([`5c7739a`](https://github.com/mraniki/talkytrend/commit/5c7739a6fa1e407e368d043957426077ee93e9c4))

* Update README.md ([`a805531`](https://github.com/mraniki/talkytrend/commit/a8055310e400765b98f7c6923ee650eec400d8af))

* Update README.md ([`f545b9a`](https://github.com/mraniki/talkytrend/commit/f545b9a603a6a7617fac032bd3974dab2a4a8fec))

* Update README.md ([`e81867a`](https://github.com/mraniki/talkytrend/commit/e81867a6318e219ae8966fc30a38496eb69083d0))

* Update README.md ([`f23472b`](https://github.com/mraniki/talkytrend/commit/f23472b320d9a05af28a198da6b54577bff83c2e))

* Update README.md ([`121efad`](https://github.com/mraniki/talkytrend/commit/121efad055308d4622aa552cf00ff007d2fc8ee8))

* Update bot.py ([`fa6e9f5`](https://github.com/mraniki/talkytrend/commit/fa6e9f5aa475a6f687b14756ba27f316b05c6931))

* Update requirements.txt ([`adf0fdd`](https://github.com/mraniki/talkytrend/commit/adf0fdd7fd9d5cd3e4f32c60def4b8874daabc1a))

* Update bot.py ([`997b5dd`](https://github.com/mraniki/talkytrend/commit/997b5ddc10daf84e9b5aa70d594a14d2d77ff9a5))

* Update bot.py ([`3e8dbe4`](https://github.com/mraniki/talkytrend/commit/3e8dbe464f77534fd1cb8f4870905c5ac7f14e70))

* Update bot.py ([`c6fcdf3`](https://github.com/mraniki/talkytrend/commit/c6fcdf36841734fb13b4228e8d277e53be059220))

* Update bot.py ([`ef55ba5`](https://github.com/mraniki/talkytrend/commit/ef55ba5ff3e79d69e785870b8d5bac119b3b255b))

* Update bot.py ([`3855b53`](https://github.com/mraniki/talkytrend/commit/3855b5348da1411c56772ab8a09e23f2cfe28e41))

* Update bot.py ([`caba8e1`](https://github.com/mraniki/talkytrend/commit/caba8e175fbb651545dde7d13b799cdcc6fcc0b3))

* Update bot.py ([`fed11a6`](https://github.com/mraniki/talkytrend/commit/fed11a6b2806f045265fea2916c24a60d475986d))

* Update bot.py ([`e696987`](https://github.com/mraniki/talkytrend/commit/e6969873a56ab136bbc57b84df0e13f23a8667f3))

* Update bot.py ([`794c35e`](https://github.com/mraniki/talkytrend/commit/794c35eaf318f270a0112f548912cb5ed744d91c))

* Update bot.py ([`843edb1`](https://github.com/mraniki/talkytrend/commit/843edb17a1a586a037b7bc00fc6261525aa87fc1))

* Update bot.py ([`f9aa40a`](https://github.com/mraniki/talkytrend/commit/f9aa40a5aeccba9d7cf80f138a913f7f99762d49))

* Update requirements.txt ([`2f69f35`](https://github.com/mraniki/talkytrend/commit/2f69f35e2444d98b2a7047bfc51161594a868078))

* Update requirements.txt ([`6c0051e`](https://github.com/mraniki/talkytrend/commit/6c0051e1424cb4104e0c556b34ddc49fc494f94a))

* Update Dockerfile ([`1e1ff65`](https://github.com/mraniki/talkytrend/commit/1e1ff65ceb717b030a30b7a2f4ef695fd51c6cfa))

* Update Dockerfile ([`b3c923a`](https://github.com/mraniki/talkytrend/commit/b3c923a046882ebb036046e071ee219c09fce60d))

* Update requirements.txt ([`8a10eb3`](https://github.com/mraniki/talkytrend/commit/8a10eb3f0c95d1ee86b0237c8f1ad0a254010857))

### 👷

* 👷 CI Change ([`9c70f78`](https://github.com/mraniki/talkytrend/commit/9c70f7832841ca6d7acc5f329617d562eef438f5))
